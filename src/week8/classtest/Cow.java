package week8.classtest;

public class Cow extends Animals{
    public Cow(String name, int id){
        super(name, id);
    }

    @Override
    public void eat() {
        System.out.println("I eat grass.");
    }

    @Override
    public void sleep() {
        System.out.println("I'm sleeping.");
    }

    @Override
    public void introduction() {
        System.out.println("I like singing!");
    }

    @Override
    public String toString() {
       String result;
       result = "My name is " + name + "\nMy id is" + id;
       return result;
    }
}
