package week8.classtest;

public abstract class Animals {
    protected String name;
    protected int id;

    public Animals(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public abstract void eat();
    public abstract void sleep();
    public abstract void introduction();
}
