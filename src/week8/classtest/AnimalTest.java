package week8.classtest;

public class AnimalTest {
    public static void main(String[] args) {
        Cow cow = new Cow("Mike",0001);
        Sheep sheep = new Sheep("Sean", 0002);

        System.out.println(cow);
        cow.eat();
        cow.sleep();
        cow.introduction();

        System.out.println();

        System.out.println(sheep);
        sheep.eat();
        sheep.sleep();
        sheep.introduction();

        System.out.println();
        System.out.println("Mike and Sean are good friend!");
    }
}
