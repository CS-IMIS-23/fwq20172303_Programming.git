package week8.classtest;

public class Sheep extends Animals {
    public Sheep(String name, int id){
        super(name, id);
    }

    @Override
    public void eat() {
        System.out.println("I eat grass too.");
    }

    @Override
    public void sleep() {
        System.out.println("I'm not sleeping.");
    }

    @Override
    public void introduction() {
        System.out.println("I like dancing!");
    }

    @Override
    public String toString() {
        String result;
        result = "My name is " + name + "\nMy id is" + id;
        return result;
    }
}
