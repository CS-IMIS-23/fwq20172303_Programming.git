package week8;

import week6.DVDCollection;

public class PP105 {
    public static void main(String[] args) {
        DVD[] movies = new DVD[7];

        movies[0] = new DVD("The Godfather", "Francis Ford Coppola", 1972, 24.95, true);
        movies[1] = new DVD("District 9", "Neill Blomkamp", 2009, 19.95, false);
        movies[2] = new DVD("Iron Man", "Jon Favreau", 2008, 15.95, false);
        movies[3] = new DVD("All About Eve", "Jseph Mankiewicz",1950,17.50,false);
        movies[4] = new DVD("The Matrix","Andy&Lana Wachowski",1999,19.95,true);
        movies[5] = new DVD("Iron Man 2", "Jon Favreau", 2010, 22.99, false);
        movies[6] = new DVD("Casablanca","Micheal Curtiz", 1942, 19.95, false);

        Sorting.selectionSort(movies);

        for (DVD movie : movies) {
            System.out.println(movie);
        }
    }
}
