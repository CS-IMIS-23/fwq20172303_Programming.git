package Experiment1;

import New_week2.ArrayStack;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

public class PointFour {
    public static void main(String[] args) throws IOException {
        String str;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter some numbers: ");
        str = scanner.nextLine();
        ArrayQueue queue = new ArrayQueue();
        String[] transfer = str.split(" ");
        for (int i = 0; i < transfer.length;i++){
            queue.enqueue(Integer.parseInt(transfer[i]));
        }
        int nFanWenqi = queue.size();
        System.out.println("链表为： " + queue.toString());
        System.out.println("元素总数为： " + nFanWenqi);

        //读取文件(缓存字节流)
        BufferedInputStream in = new BufferedInputStream(new FileInputStream("E:\\java\\point2.txt"));
        //一次性取多少字节
        byte[] bytes = new byte[2048];
        //接受读取的内容(n就代表的相关数据，只不过是数字的形式)
        int n = -1;
        String a = null;
        //循环取出数据
        while ((n = in.read(bytes,0,bytes.length)) != -1) {
            //转换成字符串
            a = new String(bytes,0,n,"GBK");
        }
        int b = Integer.parseInt(String.valueOf(a.charAt(0)));
        int c = Integer.parseInt(String.valueOf(a.charAt(2)));

        queue.insert(5,b);
        System.out.println("在中间插入后链表为： " + queue.toString());
        nFanWenqi = queue.size();
        System.out.println("元素总数为： " + nFanWenqi);

        queue.insert(0,c);
        System.out.println("在开头插入后链表为： " + queue.toString());
        nFanWenqi = queue.size();
        System.out.println("元素总数为： " + nFanWenqi);

        queue.delete(5);
        System.out.println("删除元素后链表为： " + queue.toString());
        nFanWenqi = queue.size();
        System.out.println("元素总数为： " + nFanWenqi);
    }
}
