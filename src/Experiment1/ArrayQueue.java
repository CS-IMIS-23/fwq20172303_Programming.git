package Experiment1;

import java.util.Arrays;

public class ArrayQueue {
    private int[] queue;
    private int count,ADD = 100,MORE = 100;

    public ArrayQueue(){
        queue = new int[ADD];
    }

    public void expandCapacity(){
        queue = Arrays.copyOf(queue,queue.length + MORE);
    }

    public void enqueue(int element){
        if (size() == queue.length){
            expandCapacity();
        }
        queue[count] = element;
        count++;
    }

    public int size(){
        return count;
    }

    public void insert(int index,int element){
        if (index != 0){
            int i;
            for (i = count + 1; i >= index - 1; i--){
                queue[i] = queue[i - 1];
            }
            queue[i + 1] = element;
        }
        else {
            for (int i = count + 1;i > 0;i--){
                queue[i] = queue[i - 1];
            }
            queue[0] = element;
        }
        count++;
    }

    public void delete(int index){
        for (int i = index;i < count;i++){
            queue[i] = queue[i + 1];
        }
        count--;
    }

    public void Sort(){
        for (int i = 0;i < count;i++){
            int k = i;
            //找出最小值
            for (int j = i + 1;j < count;j++){
                if (queue[k] > queue[j]){
                    k = j;
                }
            }
            //进行排序
            if (k > i){
                int temp = queue[i];
                queue[i] = queue[k];
                queue[k] = temp;
            }
            int b = i + 1;
            System.out.println("第" + b + "次排序后的数组为： ");
            for (int a = 0;a < count;a++){
                String str = "";
                str += queue[a] + " ";
                System.out.print(str);
            }
            System.out.println();
            System.out.println("元素总数为： " + count);
        }
    }

    @Override
    public String toString() {
        String str = "";
        for (int i = 0;i < count;i++){
            str += queue[i] + " ";
        }
        return str;
    }
}
