package New_week2;



public class LindedStack<T> implements StackADT<T> {
    private int count;
    private LinearNode<T> top;//设置栈顶，同时也是一个节点
    //构造函数，用于初始化
    public LindedStack(){
        count = 0;
        top = null;
    }

    @Override
    public void push(T element) {
        LinearNode<T> temp=new LinearNode<T>(element);
        //其实就是一个插入操作
        temp.setNext(top);
        top = temp;
        count ++;
    }

    @Override
    public void expandCapacity() {

    }


    @Override
    public T pop() throws EmptyCollectionException {

        if (isEmpty()) {
            throw new EmptyCollectionException("the stack is empty!");
        }

        T result = top.getElement();
        top=top.getNext();
        count--;

        return result;
    }

    @Override
    public T peek() {
        if (isEmpty()) {
            throw new EmptyCollectionException("Stack");
        }
        T result = top.getElement();

        return result;
    }

    @Override
    public boolean isEmpty() {
        if (size()==0){
            return true;
        }
        else
        {
            return false;
        }
    }

    @Override
    public int size() {
        return count;
    }
    @Override
    public String toString(){
        LinearNode node = top;
        String result = "";
        int a = count;
        while (a > 0) {
            result += node.getElement()+" ";
            node = node.getNext();
            a--;
        }
        return result;
    }
}