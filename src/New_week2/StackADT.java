package New_week2;

public interface StackADT<T> {
    public void push (T element);

    public void expandCapacity();

    public T pop();

    public T peek();

    public boolean isEmpty();

    public int size();

    @Override
    public String toString();
}
