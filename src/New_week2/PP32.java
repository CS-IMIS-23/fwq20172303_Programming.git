package New_week2;

import java.util.Scanner;
import java.util.StringTokenizer;

public class PP32 {
    public static void main(String[] args){
        //输入句子
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a sentence: ");
        String sentence = scanner.nextLine();
        //使用transfer类进行颠倒顺序
        Transfer(sentence);
    }

    public static void Transfer(String str){
        ArrayStack stack = new ArrayStack();
        StringTokenizer tokenizer = new StringTokenizer(str);
        while (tokenizer.hasMoreTokens()){//循环获取句子中的单词
            String transfer = tokenizer.nextToken();//获取单词
            for (int i = 0;i < transfer.length();i++){
                stack.push(transfer.charAt(i));//单词的字符入栈
            }
            int j = 0;
            String s = "";
            while (j < transfer.length()){
                s += stack.pop() + "";//字符出栈以实现倒叙效果
                j++;
            }
            System.out.print(s + " ");//输出
        }
    }
}
