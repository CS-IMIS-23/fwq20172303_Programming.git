package New_week2;

public class LinearTester {
    public static void main(String[] args) {
        LindedStack lindedStack = new LindedStack();
        System.out.println("未输入前的栈测试是否为空:");
        System.out.println(lindedStack.isEmpty());
        lindedStack.push(111);
        lindedStack.push(222);
        lindedStack.push(333);
        System.out.println("输入后测试栈是否为空：");
        System.out.println(lindedStack.isEmpty());
        System.out.println("栈的元素个数： "+lindedStack.size());
        System.out.println("栈顶元素："+lindedStack.peek());
        lindedStack.pop();
        System.out.println("pop后栈顶元素： "+lindedStack.peek());
        System.out.println("toString测试： " + lindedStack.toString());
    }
}
