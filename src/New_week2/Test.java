package New_week2;

public class Test {
    public static void main(String[] args) {
        ArrayStack stack = new ArrayStack();
        stack.push(6);
        stack.push(13);
        stack.push(1999);
        //完成peek、isEmpty、size和toString方法的测试。
        System.out.println("栈顶元素为： " + stack.peek());
        System.out.println("栈是否为空： " + stack.isEmpty());
        System.out.println("栈内的元素个数为： " + stack.size());
        System.out.println(stack.toString());
    }
}
