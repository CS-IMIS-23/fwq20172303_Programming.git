package New_week2.链表栈;

public class LinkedListExample {
    public static void main(String[] args) {
        Student student = new Student("Tim",20172301,"study");
        Student head = student;//形成链表
        PrintLinkedList(head);//输出链表
    }

    //可以在本类里调用，如果不加static则不行
    public static void PrintLinkedList(Student head){
        Student node = head;
        while (node != null){
            System.out.println("name: " + node.name + "\nnumber: " + node.number + "\nhobby: " + node.hobby);
            node = node.next;
        }
    }

    //删除结点
    public static void DeleteNode(Student head,Student node){
        Student pre = head, current = head;
        while (current != null){
            if (current.number != node.number){
                pre = current;
                current = current.next;
            }
        }
        pre.next = current.next;
    }
    //插入结点
    public static void  InsertNode(Student head,Student node){
        //尾插法
        Student temp = head;
        while (temp.next != null){
            temp = temp.next;
        }
        temp.next = node;
        //头插法
//        node.next = head;
//        head = node;
    }
    //在中间插入
    public static void InsertNode(Student head,Student node1,Student node3){
        Student point = head;
        while((point.number != node1.number) && point != null){
            point = point.next;
        }
        if (point.number == node1.number){
            node3.next = point.next;
            point.next = node3;
        }
    }
}
