package New_week2.链表栈;

public class Student {
    //protected：在同一个包内可以访问
    //private:类以外访问不到
    //public:包内包外都能用
    protected String name;
    protected int number;
    protected String hobby;

    //定义一个结点
    protected Student next = null;

    //生成构造函数
    public Student(String name, int number, String hobby) {
        this.name = name;
        this.number = number;
        this.hobby = hobby;
    }
}
