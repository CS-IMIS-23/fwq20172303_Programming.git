package New_week2.Afterclasstest;

import java.util.ArrayList;
import java.util.Scanner;

/*
@date:2018/9/15 @author:Frame
*/
public class NumberList {
    public static void main(String[] args) {
        int number, i = 0;
        String anwser = "y";
//        int[] a = new int[100];
        ArrayList a = new ArrayList();
        while (anwser.equalsIgnoreCase("y")){
            System.out.print("Enter a number: ");
            Scanner scanner = new Scanner(System.in);
            number = scanner.nextInt();
            a.add(number);
            i++;
            System.out.print("Do you want to enter again? (y/n)");
            anwser = scanner.next();
        }
//        Number head = new Number(a[0]);
//        Number node = head;
//        for (int b = 0; b < a.length; b++){
//            Number temp = new Number(a[b]);
//            node.next = temp;
//            node = temp;
//        }

        Number head = new Number((Integer) a.get(0));
        Number node = head;
//        int b = 0;
//        while (node != null && b <= a.size()){
//            node.number = (int) a.get(b);
//            node = node.next;
//            b++;
//        }
        for (int b = 1;b < a.size();b++){
            Number temp = new Number((Integer) a.get(b));
            node.next = temp;
            node = temp;
        }
        Print(head);
        Number number1 = new Number(23);
        Number number2 = new Number(345);
        Number number3 = new Number(67);
        Insert(head,number1);
        Insert(2,head,number2);
        Delete(3,head);
        Add(head,number3);
        System.out.println(" ");
        Print(head);
        Sort(head);
        System.out.println(" ");
        Print(head);
    }
    //在开头插入
    public static void Add(Number head,Number node){
        Number temp = head.next;
        node.next = temp;
        head.next = node;
        int tem = head.number;
        head.number = node.number;
        node.number = tem;
    }
    //在结尾插入
    public static void Insert(Number head,Number node){
        Number temp = head;
        while (temp.next != null){
            temp = temp.next;
        }
        temp.next = node;
    }
    //在中间插入（在输入的index之前插入）
    public static void Insert(int index,Number head,Number node){
        int a = 0;
        Number temp = head;
        while (temp != null && a != index - 2){
            a++;
            temp = temp.next;
        }
        node.next = temp.next;
        temp.next = node;
        //当index小于2时，直接将前两个结点的number交换
        if (index < 2){
            int tem = head.number;
            head.number = node.number;
            node.number = tem;
        }
    }
    //删除结点
    public static void Delete(int index,Number head){
        Number current = head;
        int a = 0;
        while (current != null && a != index - 2){
             current = current.next;
             a++;
        }
        current.next = current.next.next;
    }
    //打印
    public static void Print(Number head){
        Number node = head;
        while (node != null){
            System.out.print(node.number + " ");
            node = node.next;
        }
    }
    //使用冒泡排序法进行排序
    public static Number Sort(Number head){
        Number node = head,current = null;
        //当链表为空或仅有一个结点时
        if (head == null || head.next == null) {
            return head;
        }
        while (node.next != current){
            while (node.next != current){
                if (node.number > node.next.number){
                    int temp = node.number;
                    node.number = node.next.number;
                    node.next.number = temp;
                }
                node = node.next;
            }
            current = node;//使下一次遍历的尾结点为当前结点
            node = head;
        }
        return head;
    }
}

