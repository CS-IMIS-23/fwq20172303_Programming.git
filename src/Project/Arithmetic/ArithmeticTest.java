package Project.Arithmetic;

import java.text.NumberFormat;
import java.util.Scanner;

public class ArithmeticTest {
    public static void main(String[] args) {
        Scanner number = new Scanner(System.in);
        Scanner result = new Scanner(System.in);
        NumberFormat fmt = NumberFormat.getPercentInstance();
        MakeQuestions Nq = new MakeQuestions();
        Calculator jdg = new Calculator();
        InfixToSuffix its = new InfixToSuffix();
        int count;//  题目数
        int level;//  级别
        String expr;//  题目
        while(1>0) {
            int j = 0;
            System.out.print("请输入要生成的题目数：" );
            count = number.nextInt();
            while (count == 0)
            {
                System.out.println("错误，请输入有效数字！(最小为1，理论无上限)");
                System.out.print("请输入要生成的题目数：");
                count = number.nextInt();
            }
            System.out.print("请输入生成题目的级别（每增加一级多一个运算符，最低为一级）：");
            level = number.nextInt();
            while (level == 0)
            {
                System.out.println("错误，请输入有效数字！(最小为1，理论无上限)");
                System.out.print("请输入生成题目的级别（每增加一级多一个运算符，最低为一级）：");
                level = number.nextInt();
            }
            for (int i = 0; i < count; i++) {
                //生成题目
                int a;
                a = i + 1;
                expr = Nq.getExper(level);
                String s = "题目" + a + ":" + expr + " =";
                System.out.print(s);//输入题目

                String result1 = result.nextLine();
                its.conversion(expr);
                System.out.println(its.getMessage());

                if (result1.equals(jdg.evaluate(its.getMessage()))){
                    System.out.println("正确！");
                    j++;
                }
                else
                    System.out.println("错误，正确答案为：" + jdg.evaluate(its.getMessage()));
            }
            double accuracy = (double)j/count;
            System.out.println("完成" + count + "道题目，正确率为" + fmt.format(accuracy));
            System.out.println("答对"+ j + "道题");
            String s1 = number.nextLine();
            System.out.print("是否继续生成题目？（y/n）（之前统计将清零）:"  );
            String s2 = number.nextLine();
            if (s2.equalsIgnoreCase("n")){
                break;
            }
        }
    }
}