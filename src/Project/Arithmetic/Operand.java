package Project.Arithmetic;

import java.util.Random;

public class Operand {
    private Random rnd1 = new Random();
    private Random rnd2 = new Random();
    private Random rnd3 = new Random();
    private RationalNumber b;
    private String  a, op1, op2, op3;
    private int c, d;


    //  Random access to an integer and convert it to String type.
    private String  getA()
    {
        a = String.valueOf(rnd1.nextInt(10) + 1);
        return a;
    }
    //  Random access to a true fraction
    private RationalNumber getB(){
        while (1>0) {
            c = rnd1.nextInt(10) + 1;
            d = rnd2.nextInt(10) + 1;
            b = new RationalNumber(c, d);
            if (c < d)
            {
                break;
            }
        }
        return b;
    }
    public Operand(){

    }
    //  A random number of operands may be integers that may be fractions.
    public String getOp1(){
        if (rnd3.nextInt(2) == 0){
            op1 = getA();
        }
        else
            op1 = getB().toString();
        return op1;
    }
    public String getOp2(){
        if (rnd3.nextInt(2) == 0){
            op2 = getA();
        }
        else
            op2 = getB().toString();
        return op2;
    }
    public String getOp3(){
        if (rnd3.nextInt(2) == 0){
            op3 = getA();
        }
        else
            op3 = getB().toString();
        return op3;
    }
}