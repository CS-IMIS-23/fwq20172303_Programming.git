package Project.Arithmetic;

import java.util.Random;

public class MakeQuestions {
    private Random  r1 = new Random();
    private Operand opd = new Operand();
    private String operator;
    private String expr;

    //  Random access operator
    private String getOperator(){
        int i = r1.nextInt(4);
        if (i == 0){
            operator = " + ";
        }
        else if (i == 1){
            operator = " - ";
        }
        else if (i == 2){
            operator = " * ";
        }
        else if (i == 3){
            operator = " ÷ ";
        }
        return operator;
    }
    //  Generation of topics
    public String getExper(int i) {
        //  Get multiple operators in a loop method
        expr = opd.getOp1() + getOperator();
        int a  = i;
        for (int j = 0; j < a - 1; j++) {
            String s = opd.getOp1() + getOperator();
            expr += s;

            Random c = new Random();

            int d = c.nextInt(2);
            String t;
            while (d == 0 ) {
                if (a < 2){
                    break;
                }
                t = " ( " + opd.getOp1() + getOperator() + opd.getOp2();

                expr += t + " ) " + getOperator();
                a = a - 2;
                break;
            }

        }
        expr = expr + opd.getOp3();
        return expr;
    }
}
