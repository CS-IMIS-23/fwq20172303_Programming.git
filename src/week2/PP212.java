//This is a program that calculates the length and area of a square.
//My English is poor.

import java.util.Scanner;

public class PP212
{
  public static void main(String[] args)

  {
    int number;
    double length, area;

    Scanner scan = new Scanner(System.in);

    System.out.print("Enter the length number of a square: ");
    number = scan.nextInt();

    length = number*4;

    area = number*number;

    System.out.println("The length of the square: " + length);
    System.out.println("The area of the square: " + area);
  }
}

