//***************************************************************
//  Roses.java
//
//  Demonstrates the use of escape sequences.
//***************************************************************

public class Roses
{
  //------------------------------------------------------------
  // Prints a poem (of sorts) on multiple lines.
  //------------------------------------------------------------
  public static void main(String[] args)
  {
     System.out.println("Roses are red,\n\tViolets are blue.\n" +
         "Suger is sweet,\n\tBut I have \"commitment issues\",\n\t" +
         "So I'd rather just be friends\n\tAt this point in our " +
         "relationship.");
  //-----------------------------------------------------------
  // A test which uses to compare \n to \r
  //----------------------------------------------------------
     System.out.print("Test1 \rlalalalala");

     System.out.print("Test2 \nlalalalala");

     System.out.print("Test3 \r\nlalalalala");

     System.out.print("Test4 \n\rlalalalala");
  }
}

