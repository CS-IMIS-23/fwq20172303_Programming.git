package week5;
//*************************************************************
// Make a coinfilp game, input the result everytime.
// Calculate the time of head.
//*************************************************************
class CountFilps {
    public static void main(String[] args) {
        Coin myCoin = new Coin();

        int time = 0, head = 0;
        while (time <= 100){
            myCoin.flip();
            if (myCoin.isHeads()){
                System.out.println("You win.");
                head++;
                time++;}
            else{
                System.out.println("Better luck next time.");
                time++;}
        }
        System.out.println("The time of head is: " + head);
    }
}
