//************************************************************
// Make a stone-scissors-cloth game.
// It's a annoying program.
// You don't want to know I spend how much time to make it.
// Haha.
//***********************************************************
import java.util.*;

public class PP57 {
    //----------------------------------------------------------
    // Plays a s-s-c game with the user.
    //----------------------------------------------------------
    public static void main(String[] args) {
        int comp;
        final int A = 3;
        Random generator = new Random();
        comp = generator.nextInt(A);

        int user;
        Scanner scan = new Scanner(System.in);
        System.out.println("Let's play a stone-scissors-cloth game. " +
                          "\nIf you want to choose stone, please enter 0." +
                          "\nIf you want to choose scissors, please enter 1." +
                          "\nIf you want to choose cloth, please enter 2." );

        String another = "y";

        int win = 0, lose = 0, draw = 0;

        while (another.equalsIgnoreCase("y")){
            System.out.print("So which one do you choose: " );
            user = scan.nextInt();

            if (user >= 1.5){
                if (comp <= 0.5){
                    System.out.println("Lucky, you win!");
                    win++;}
                if (comp >= 0.5 && comp <= 1.5){
                    System.out.println("Sorry, you lose.");
                    lose++;}
                if (comp >= 1.5){
                    System.out.println("That's a draw.");
                    draw++;}
            }
            else if (user >= 0.5 && user <= 1.5){
                    if (comp <= 0.5){
                        System.out.println("Sorry, you lose.");
                        lose++;}
                     if (comp >= 0.5 && comp <= 1.5){
                        System.out.println("That's a draw.");
                        draw++;}
                    if (comp >= 1.5){
                        System.out.println("Lucky, you win!");
                        win++;}
                }
            else if (user <= 0.5){
                if (comp <= 0.5){
                    System.out.println("That's a draw.");
                    draw++;}
                if (comp >= 0.5 && comp <= 1.5){
                    System.out.println("Lucky, you win!");
                    win++;}
                if (comp >= 1.5){
                    System.out.println("Sorry, you lose.");
                    lose++;}
                }
            else
                System.out.println("Invalid input.");
                System.out.println("The computer choose: " + comp);

                System.out.println();
                System.out.print("Do you want to play again? (y/n)");
                another = scan.next();
        }
        System.out.println("The round you win is: " + win +
                            "\nThe round you lose is: " + lose +
                            "\nThe round of draw is: " + draw);
    }
}
