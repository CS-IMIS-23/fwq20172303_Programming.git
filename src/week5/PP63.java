//********************************************************
// Output a 12*12 MultiplicationTables.
//*******************************************************
public class PP63 {
    public static void main(String[] args) {
        //x is chengji. a and b is chengshu.
        int x = 0;
        for (int a = 1; a <= 12; a++){
            for (int b = 1; b <= a; b++){
                x = a * b;
                System.out.print(b + "*" + a + "=" + x + "\t");
            }
            System.out.println();
        }
    }
}
