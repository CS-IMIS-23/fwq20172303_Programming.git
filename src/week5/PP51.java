package week5;
import java.util.Scanner;

public class PP51 {
    //------------------------------------------------------------
    // Reads the year and judge it.
    //------------------------------------------------------------
    public static void main(String[] args) {
        float a, b, c;
        final int STANDARD = 1582; // standards year

        Scanner scan = new Scanner(System.in);
        System.out.print("Enter the number of year: " );
        int year = scan.nextInt();
        System.out.println();

        a = year % 100;
        b = year % 400;
        c = year % 4;

        if (year < STANDARD)
            System.out.println("Invalid input.");
        else
            if (a == 0)
                if (b == 0)
                    System.out.println("The year is a bissextile.");
                else
                    System.out.println("The year is NOT a bissextile.");
            else
                if (c == 0)
                    System.out.println("The year is a bissextile.");
                else
                    System.out.println("The year is NOT a bissextile.");
    }
}
