package week5;

import java.util.Scanner;

public class PP53 {
    //-----------------------------------------------------------
    //Calculate the number of odd number,even number and zero.
    //-----------------------------------------------------------
    public static void main(String[] args) {
        String str;
        int odd = 0, even = 0, zero = 0;

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter an integer: ");
        str = scan.nextLine();

        int a = str.length();
        int b = 0;

        for (int i = 0; i < a; i++) {
            b = Integer.valueOf(str.substring(i , i + 1));
            if (b == 0) {
                zero++;
            }
            else
            if (b % 2 != 0 && b != 0) {
                odd++;
            }
            else
            if (b % 2 == 0 && b != 0) {
                even++;
            }
        }

        System.out.println("The number of odd number is: " + odd);
        System.out.println("The number of even number is " + even);
        System.out.println("The number of zero number is " + zero);

    }
}
