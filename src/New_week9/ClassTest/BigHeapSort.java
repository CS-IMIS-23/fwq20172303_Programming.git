package New_week9.ClassTest;

import java.util.Arrays;

public class BigHeapSort<T> {
    public void BigHeapSort(String[] data){
        BigArrayHeap<String> temp = new BigArrayHeap<String>();

        for (int i = 0; i < data.length; i++){
            temp.addElement(data[i]);
        }
        String str = "";
        int count = 0;
        System.out.println("-------------排序过程--------------");
        while (!temp.isEmpty()){
            data[count] = temp.removeMax();
            count++;
            System.out.println(Arrays.toString(data));
        }
        for(String i:data){
            str += i + " ";
        }
        System.out.println("-------------排序过程--------------");
        System.out.println("排序结果为： " + str);
    }
}
