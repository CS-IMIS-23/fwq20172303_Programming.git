package New_week9.ClassTest;

import New_week2.EmptyCollectionException;
import New_week7.BinaryTreeNode;
import New_week9.ArrayBinaryTree;
import New_week9.ArrayHeap;
import New_week9.HeapADT;

import java.util.Iterator;

public class BigArrayHeap<T> extends ArrayBinaryTree<T> implements BigHeapADT<T> {
    public BigArrayHeap(){super();}

    @Override
    public void addElement(T obj) {
        if (count == tree.length) {
            expandCapacity();
        }

        tree[count] = (T) obj;
        count++;
        modCount++;

        if (count > 1) {
            heapifyAdd();
        }
    }

    private void heapifyAdd()
    {
        T temp;
        int next = count - 1;

        temp = tree[next];

        while ((next != 0) &&
                (((Comparable)temp).compareTo(tree[(next-1)/2]) > 0))
        {

            tree[next] = tree[(next-1)/2];
            next = (next-1)/2;
        }

        tree[next] = temp;
    }

    @Override
    public T removeMax() throws EmptyCollectionException{
        if (isEmpty()) {
            throw new EmptyCollectionException("ArrayHeap");
        }

        T maxElement = tree[0];
        tree[0] = tree[count-1];
        heapifyRemove();
        count--;
        modCount--;

        return maxElement;
    }

    private void heapifyRemove()
    {
        T temp;
        int node = 0;
        int left = 1;
        int right = 2;
        int next;

        if ((tree[left] == null) && (tree[right] == null)) {
            next = count;
        } else if (tree[right] == null) {
            next = left;
        } else if (((Comparable)tree[left]).compareTo(tree[right]) > 0) {
            next = left;
        } else {
            next = right;
        }
        temp = tree[node];

        while ((next < count) &&
                (((Comparable)tree[next]).compareTo(temp) > 0))
        {
            tree[node] = tree[next];
            node = next;
            left = 2 * node + 1;
            right = 2 * (node + 1);
            if ((tree[left] == null) && (tree[right] == null)) {
                next = count;
            } else if (tree[right] == null) {
                next = left;
            } else if (((Comparable)tree[left]).compareTo(tree[right]) > 0) {
                next = left;
            } else {
                next = right;
            }
        }
        tree[node] = temp;
    }

    @Override
    public T findMax() throws EmptyCollectionException{
        if (isEmpty()) {
            throw new EmptyCollectionException("ArrayHeap");
        }

        return tree[0];
    }

    public T getTreePoint(int i){
        return tree[i];
    }
}
