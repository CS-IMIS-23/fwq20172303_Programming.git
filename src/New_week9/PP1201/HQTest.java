package New_week9.PP1201;

public class HQTest {
    public static void main(String[] args) {
        HeapQueue queue = new HeapQueue();
        queue.enqueue(7);
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        queue.enqueue(4);
        queue.enqueue(5);
        queue.enqueue(6);

        System.out.println(queue.size());
        System.out.println(queue.isEmpty());
        System.out.println(queue.first());
        System.out.println(queue.toString());
    }
}
