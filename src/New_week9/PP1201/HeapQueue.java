package New_week9.PP1201;

import New_week9.ArrayHeap;
import New_week9.LinkedHeap;

public class HeapQueue<T> extends ArrayHeap<T> implements HeapQueueADT<T>,Comparable<HeapQueue>{
    private int num;

    public HeapQueue(){
        super();
    }

    @Override
    public void enqueue(T element) {
        super.addElement(element);
    }

    @Override
    public T dequeue() {
        return super.removeMin();
    }

    @Override
    public T first() {
        return super.findMin();
    }

    @Override
    public boolean isEmpty() {
        return super.isEmpty();
    }

    @Override
    public int size() {
        return super.size();
    }

    @Override
    public String toString() {
        String result = "";
        int a = count;
        for (int i = 0;i < a;i++){
            result += tree[i] + " ";
        }
        return result;
    }

    @Override
    public int compareTo(HeapQueue o) {
        if (num < o.num){
            return  -1;
        }
        else {
            return 1;
        }
    }
}
