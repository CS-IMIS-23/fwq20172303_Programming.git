package New_week9;

public class PP1208 {
    public static void main(String[] args) {
        ArrayHeap heap = new ArrayHeap();
        heap.addElement(45);
        heap.addElement(33);
        heap.addElement(56);
        heap.addElement(78);
        heap.addElement(12);

        System.out.println("堆的最小值为： " + heap.findMin());
        System.out.println("堆的大小为： " + heap.size());
        System.out.println("堆为： " + heap.toString());
        heap.removeMin();
        System.out.println("删除最小元素后的堆为：" + heap.toString());
    }
}
