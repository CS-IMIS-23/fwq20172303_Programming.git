package week3;//This is a program that can make a random telephone number which have ten numbers.
//The first second and third number don't have 8 or 9.
//The fourth fifth and sixth number can't be larger than 655.

import java.util.Random;

public class PP33
{
  public static void main(String[] args)
  {
     Random generator = new Random();
     int num1, num2, num3, num4, num5, num6, num7, num8;

     num1 = generator.nextInt(8);
     num2 = generator.nextInt(8);
     num3 = generator.nextInt(8);
     num4 = generator.nextInt(556) + 100;
     num5 = generator.nextInt(10);
     num6 = generator.nextInt(10);
     num7 = generator.nextInt(10);
     num8 = generator.nextInt(10);

     System.out.println(""+ num1 + num2 + num3 + "-" + num4 + "-" + num5 + num6 +num7 + num8);
  }
} 
