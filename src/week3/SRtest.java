package week3;

import java.text.NumberFormat;
import java.util.Scanner;

public class SRtest {
    //SRweek3
    public static void main(String[] args) {
        //SR36
        String s1 = "Amanda";
        String s2 = "Bobby";
        String s3 = "Chris";

        System.out.println(s1);
        s1 = s3.toLowerCase();
        System.out.println(s2.replace('B', 'M'));
        s3 = s2.concat(s1);
        System.out.println(s3);

        //SR37
        String a1 = "Foundations";
        String a2;
        System.out.println(a1.charAt(1));
        a2 = a1.substring(0, 5);
        System.out.println(a2);
        System.out.println(a1.length());
        System.out.println(a2.length());

        //SR3.26
        double cost;

        Scanner scan = new Scanner(System.in);

        System.out.print("Enter the cost: ");
        cost = scan.nextDouble();

        NumberFormat moneyFormate = NumberFormat.getCurrencyInstance();
        System.out.println("The cost is: " + moneyFormate.format(cost));
    }
}
