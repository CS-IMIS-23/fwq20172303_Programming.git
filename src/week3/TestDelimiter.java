package week3;
//*************************************************************************
// > File Name: TestDelimiter.java
//> Created Time: 2018年03月19日 星期一 09时49分14秒
//************************************************************************

import java.util.Scanner;
    public class TestDelimiter{
        public static void main(String []args){
            int temp1,temp2;

            Scanner scan = new Scanner(System.in);
            scan.useDelimiter("#");
            System.out.println("Please input a two Integer(such as: 12#34#):");
            temp1 = scan.nextInt();
            temp2 = scan.nextInt();
            System.out.println("The first Integer is:"+temp1);
            System.out.println("The second Integer is:"+temp2);
        }
    }

