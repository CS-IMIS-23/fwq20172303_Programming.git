package week3;//This is a program that can make a random number between 20 and 40
//It can also show its sin cos and tan.

import java.util.Random;

public class PP38

{
  public static void main(String[] args)
  {
    Random generator = new Random();
    int num1;
    double a, b, c;
    
    num1 = generator.nextInt(21) + 20;
    System.out.println("The random number is: " + num1);

    a = Math.sin(num1);
    System.out.println("The sin of the random number is: " + a);

    b = Math.cos(num1);
    System.out.println("The cos of the random number is: " + b);

    c = Math.tan(num1);
    System.out.println("The tan of the random number is: " + c);
   }
}



