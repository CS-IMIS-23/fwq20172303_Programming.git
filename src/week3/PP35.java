package week3;//This is a program that can calculate the distance between two dots.

import java.util.Scanner;

public class PP35
{
  public static void main(String[] args)
  {
    double a, b, c, d, e;
    
    Scanner scan = new Scanner(System.in);

    System.out.print("Enter the first X: ");
    a = scan.nextDouble();

    System.out.print("Enter the second X: ");
    b = scan.nextDouble();

    System.out.print("Enter the first Y: ");
    c = scan.nextDouble();

    System.out.print("Enter the second Y: ");
    d = scan.nextDouble();
    
    e = Math.sqrt((a - b)*(a - b) + ( c - d)*(c - d));
    System.out.println("The distance is: " + e);
  }
}

