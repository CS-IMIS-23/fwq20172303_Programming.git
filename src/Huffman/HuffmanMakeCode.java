package Huffman;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class HuffmanMakeCode {
    public static char[] word = new char[]{'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s'
            ,'t','u','v','w','x','y','z',' '};
    public static int[] number = new int[]{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

    public static String makecode(FileInputStream stream) throws IOException {
        //读取文件(缓存字节流)
        BufferedInputStream in = new BufferedInputStream(stream);
        //一次性取多少字节
        byte[] bytes = new byte[2048];
        //接受读取的内容(n就代表的相关数据，只不过是数字的形式)
        int n = -1;
        String a = null;
        //循环取出数据
        while ((n = in.read(bytes, 0, bytes.length)) != -1) {
            //转换成字符串
            a = new String(bytes, 0, n, "GBK");
        }

        // 对文件内容进行计数
        count(a);

        return a;
    }

    // 实现对文件内容计数，内层循环依次比较字符串中的每个字符与对应字符是否相同，相同时计数；外层循环指定对应字符从a至空格
    public static void count(String str){
        for (int i = 0;i < 27;i++){
            int num = 0;
            for (int j = 0;j < str.length();j++){
                if (str.charAt(j) == word[i]){
                    num++;
                }
            }
            number[i] += num;
        }
    }

    public static char[] getWord() {
        return word;
    }

    public static int[] getNumber() {
        return number;
    }
}
