package Huffman;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HuffmanTest {
    public static void main(String[] args) throws IOException {
        // 读取文件
        FileInputStream stream = new FileInputStream("C:\\Users\\45366\\IdeaProjects\\fwq20172303_Programming\\HuffmanTest.txt");
        String a = HuffmanMakeCode.makecode(stream);
        System.out.println("读取文件内容： " + a);

        // 输出计数结果
        System.out.println("=====================计数结果========================");
        char[] chars = HuffmanMakeCode.getWord(); // 存放每个字母（便于输出）
        int[] num = HuffmanMakeCode.getNumber(); // 存放每个字母出现的次数
        for (int i = 0;i < num.length;i++){
            System.out.println(chars[i] + "出现的次数为： " + num[i] + "次");
        }
        System.out.println("=====================计数结果========================");

        // 构建哈夫曼树并进行编码
        List<HuffmanNode<String>> list = new ArrayList<>();

        for (int i = 0;i < num.length;i++){
            list.add(new HuffmanNode(num[i],chars[i]));
        }
        Collections.sort(list); // 对列表进行排序
        HuffmanNode<String> root = HuffmanTree.createTree(list);
//
        // 利用BFS遍历树并进行赋值
        List<HuffmanNode> list1;
        list1 = HuffmanTree.BFS(root);
        List<String> list2 = new ArrayList<>(); // 用于存储每个结点的字母
        List<String> list3 = new ArrayList<>(); // 用于存储每个结点的编码
        for (int i = 0;i < list1.size();i++){
            if (list1.get(i).getWord() != null){
                list2.add(String.valueOf(list1.get(i).getWord()));
                list3.add(list1.get(i).getCode());
            }
        }

        // 输出编码
        Collections.sort(list2);
        for (int i = 0;i < list3.size();i++){
            System.out.println(list2.get(i) + "的编码为： " + list3.get(i));
        }
        String result = "";
        for (int i = 0;i < a.length();i++){
            for (int j = 0; j < list2.size();j++){
                if (a.charAt(i) == list2.get(j).charAt(0)){
                    result += list3.get(j);
                }
            }
        }
        System.out.println("文件编码结果为： " + result);
        System.out.println("=====================解码结果========================");

        // 进行解码
        List<String> list4 = new ArrayList<>();
        for (int i = 0;i < result.length();i++){
            list4.add(result.charAt(i) + "");
        }
        String temp = "";
        String temp1 = "";
        while (list4.size() > 0){
            temp += "" + list4.get(0);
            list4.remove(0);
            for (int i = 0;i < list3.size();i++){
                if (temp.equals(list3.get(i))){
                    temp1 += "" + list2.get(i);
                    temp = "";
                }
            }
        }
        System.out.println("文件解码结果为： " + temp1);

        // 写入文件
        File file = new File("C:\\Users\\45366\\IdeaProjects\\fwq20172303_Programming\\HuffmanTest2.txt");
        Writer out = new FileWriter(file);
        out.write(result);
        out.close();
    }
}
