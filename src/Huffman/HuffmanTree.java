package Huffman;

import New_week3.QueueADT;
import New_week4.ArrayList;
import New_week6.Sorting;
import New_week9.HeapSort;

import java.util.ArrayDeque;
import java.util.Collections;
import java.util.List;
import java.util.Queue;

public class HuffmanTree {
    public static HuffmanNode createTree(List<HuffmanNode<String>> nodes) {
        while (nodes.size() > 1){
            // 对数组进行排序
            Collections.sort(nodes);
            // 当列表中还有两个以上结点时，构造树
            // 获取权值最小的两个结点
            HuffmanNode left = nodes.get(nodes.size() - 2);
            left.setCode(0 + "");
            HuffmanNode right = nodes.get(nodes.size() - 1);
            right.setCode(1 + "");
            // 生成新的结点，新结点的权值为两个子节点的权值之和
            HuffmanNode parent = new HuffmanNode(left.getWeight() + right.getWeight(), null);
            // 使新结点成为父结点
            parent.setLeft(left);
            parent.setRight(right);
            // 删除权值最小的两个结点
            nodes.remove(left);
            nodes.remove(right);
            nodes.add(parent);
        }
        return nodes.get(0);
    }

    // 广度优先遍历
    public static List<HuffmanNode> BFS(HuffmanNode root){
        Queue<HuffmanNode> queue = new ArrayDeque<HuffmanNode>();
        List<HuffmanNode> list = new java.util.ArrayList<HuffmanNode>();

        if (root != null){
            // 将根元素加入队列
            queue.offer(root);
            root.getLeft().setCode(root.getCode() + "0");
            root.getRight().setCode(root.getCode() + "1");
        }

        while (!queue.isEmpty()){
            // 将队列的队尾元素加入列表中
            list.add(queue.peek());
            HuffmanNode node = queue.poll();
            // 如果左子树不为空，将它加入队列并编码
            if (node.getLeft() != null){
                queue.offer(node.getLeft());
                node.getLeft().setCode(node.getCode() + "0");
            }
            // 如果右子树不为空，将它加入队列并编码
            if (node.getRight() != null){
                queue.offer(node.getRight());
                node.getRight().setCode(node.getCode() + "1");
            }
        }
        return list;
    }
}
