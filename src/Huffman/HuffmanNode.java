package Huffman;

public class HuffmanNode<T> implements Comparable<HuffmanNode<T>>{
    private double weight; //权值
    private T word; // 字母
    private HuffmanNode left;
    private HuffmanNode right;
    private HuffmanNode parent;
    String code; // 存储最后的编码

    public HuffmanNode(double weight,T word){
        this.weight = weight;
        this.word = word;
        this.code = "";
    }

    public String getCode() {
        return code;
    }

    public void setCode(String str) {
        code = str;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public T getWord() {
        return word;
    }

    public void setWord(T word) {
        this.word = word;
    }

    public HuffmanNode getLeft() {
        return left;
    }

    public void setLeft(HuffmanNode left) {
        this.left = left;
    }

    public HuffmanNode getRight() {
        return right;
    }

    public void setRight(HuffmanNode right) {
        this.right = right;
    }

    public HuffmanNode getParent() {
        return parent;
    }

    public void setParent(HuffmanNode parent) {
        this.parent = parent;
    }

    @Override
    public String toString() {
        return word + " 权值： " + weight + " 左孩子： " + left + " 右孩子： " + right;
    }

    @Override
    public int compareTo(HuffmanNode<T> o) {
        if (this.getWeight() > o.getWeight()){
            return -1;
        }
        else if (this.getWeight() < o.getWeight()){
            return 1;
        }
        return 0;
    }
}
