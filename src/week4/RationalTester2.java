package week4;

public class RationalTester2 {
    public static void main(String[] args) {
        RationalNumber r1 = new RationalNumber(6, 8 );
        RationalNumber r2 = new RationalNumber(1,3);

        System.out.println("First rational number: " + r1);
        System.out.println("Second rational Number: " + r2);
        if (r1.compareTo(r2) < 0.0001)
            System.out.println("0");
        else if (r1.compareTo(r2) > 0.0001)
            System.out.println("1");
        else
            System.out.println("-1");
    }
}
