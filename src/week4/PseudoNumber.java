package week4;

import java.util.Random;
public class PseudoNumber {
    public static void main(String[] args) {
        Random generator = new Random();
        int num1;

        num1 = generator.nextInt(20) -10;
        System.out.println("A PseudoNumber: " + num1);
        System.out.println("ToHexString:0x"+Integer.toHexString(num1));
        System.out.println("TobinayString:"+Integer.toBinaryString(num1));
    }
}
