package week4;

public class PairOfDie {
    private int a, b;
    private final int MAX = 6;
    private Die c, d;

    public PairOfDie(){
        c = new Die();
        d = new Die();
    }

    public int rolling(){
        a = c.roll();
        b = d.roll();
        return a + b;
    }

    public void setA(int e){
        a = e;
    }
    public void setB(int f){
        b = f;
    }

    public int getA(){
        return a;
    }
    public int getB(){
        return b;
    }
}

