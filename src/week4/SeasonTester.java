package week4;
//******************************************************************************
// SeasonTester.java
//
// Demonstrates the use of a full enumerated type.
//******************************************************************************
public class SeasonTester {
    //-----------------------------------------------------------
    // Iterates through the values of the Season enumerted tpye.
    //-----------------------------------------------------------
    public static void main(String[] args) {
        for (Season time : Season.values())
            System.out.println(time + "\t" + time.getSpan());
    }
}
