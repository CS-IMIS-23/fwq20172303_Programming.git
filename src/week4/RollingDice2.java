package week4;

public class RollingDice2 {
    public static void main(String[] args) {
        PairOfDie die1;
        int sum;

        die1 = new PairOfDie();

        die1.rolling();
        System.out.println("Die One: " + die1.getA() + "\nDie Two: " + die1.getB());

        sum = die1.getA() + die1.getB();
        System.out.println("The sum is: " + sum);
    }
}
