package week4;

public class Car {
    private String manufacturer;
    private String type;
    private int date;
    private boolean YN;

    public Car(String a, String b, int c){
        manufacturer = a;
        type = b;
        date = c;
    }
    public String getManufacturer()
    {return manufacturer;}
    public String getType()
    {return type;}
    public int getDate()
    {return date;}

    public void setManufacturer(String d)
    { manufacturer = d;}
    public void setType(String e)
    {type = e;}
    public void setDate(int f)
    { date = f; }

    public String toString() {
      return"\nManufacturer: " + manufacturer + "\nType: " + type +"\ndate: " + date;
    }

    public boolean isAntique(){
        if (date < 1973)
        { YN = true;
    }
         else {
            YN = false;
        }
        return YN;
        }
}