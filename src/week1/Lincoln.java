//*************************************************
//  Lincoln.java    Author: Lewis/Loftus
//
// Demenstrates the basic structure of a Java application.
//*************************************************


public class Lincoln
{
  //--------------------------------------------
  // Prints a presidental quote.
  //--------------------------------------------
  public static void main(String[] args)
  {
        System.out.println("A quote by Abraham Lincoln:");

        System.out.println("Whatever you are, be a good one.");
  }
}
