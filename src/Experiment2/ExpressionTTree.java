package Experiment2;

import New_week4.ArrayUnorderedList;
import New_week7.LinkedBinaryTree;
import java.util.Iterator;
import java.util.Stack;
import java.util.StringTokenizer;


public class ExpressionTTree extends LinkedBinaryTree {
    private Stack<String> opeStack;
    private Stack<LinkedBinaryTree> numStack;
    public ExpressionTTree() {
        opeStack = new Stack<String>();
        numStack = new Stack<LinkedBinaryTree>();
    }

//    //检索并返回下一个操作数
//    private LinkedBinaryTree getOperand(Stack<LinkedBinaryTree> treeStack)
//    {
//        LinkedBinaryTree temp;
//        temp = treeStack.pop();
//
//        return temp;
//    }
//
//    public void createTree(String expression){
//        LinkedBinaryTree<String> operand1, operand2;
//        //存放操作符
//        opeStack = new Stack<String>();
//        //存放操作数
//        numStack = new Stack<LinkedBinaryTree>();
//
//        String token;
//        StringTokenizer tokenizer = new StringTokenizer(expression);
//
//        while (tokenizer.hasMoreTokens()) {
//            token = tokenizer.nextToken();
//
//            if (token.equals("(")) {
//                //  如果是左括号，入栈
//                opeStack.push(token);
//            }else if (token.equals("+") || token.equals("-")) {
//                //  如果是+或-，判断栈是否为空
//                if (!opeStack.empty()){
//                    //  如果栈不是空的，判断栈的顶部元素是什么
//                    if (opeStack.peek().equals("(")) {
//                        //  如果是左括号，操作符入栈
//                        opeStack.push(token);
//                    }else{
//                        //  否则，排出两个栈顶元素并创建树
//                        //来源：PositifixEvaluator中构造表达式树的方法
//                        String str = opeStack.pop();
//                        operand1 = getOperand(numStack);
//                        operand2 = getOperand(numStack);
//                        LinkedBinaryTree tree = new LinkedBinaryTree(str,operand2,operand1);
//                        numStack.push(tree);
//                        opeStack.push(token);
//                    }
//                }else {
//                    //  否则操作符入栈
//                    opeStack.push(token);
//                }
//            }else if (token.equals("*") || token.equals("÷")){
//                //  如果是*或÷，判断栈是否为空
//                if (!opeStack.empty()) {
//                    //  如果栈不是空的，判断栈的顶部元素是什么
//                    if (opeStack.peek().equals("*") || opeStack.peek().equals("÷")) {
//                        //  如果栈顶元素为*或÷，排出两个栈顶元素创建树
//                        String str = opeStack.pop();
//                        operand1 = getOperand(numStack);
//                        operand2 = getOperand(numStack);
//                        LinkedBinaryTree tree = new LinkedBinaryTree(str,operand2,operand1);
//                        numStack.push(tree);
//                        opeStack.push(token);
//                    }else {
//                        //  否则操作符入栈
//                        opeStack.push(token);
//                    }
//                }else {
//                    //  如果栈是空的，操作符入栈
//                    opeStack.push(token);
//                }
//            } else if (token.equals(")")) {
//                //  如果是右括号则开始循环
//                while (true) {
//                    //  删除栈顶元素并将它分配给A
//                    String A = (String) opeStack.pop();
//                    if (!A.equals("(")) {
//                        //  如果不是左括号，构建新树
//                        operand1 = getOperand(numStack);
//                        operand2 = getOperand(numStack);
//                        LinkedBinaryTree tree = new LinkedBinaryTree(A,operand2,operand1);
//                        numStack.push(tree);
//                    } else {
//                        // 如果是左括号，跳出循环
//                        break;
//                    }
//                }
//            }else {
//                //  如果是操作数，加入操作数栈
//                LinkedBinaryTree temp = new LinkedBinaryTree(token);
//                numStack.add(temp);
//            }
//        }
//        while (!opeStack.empty()) {
//            //  从栈中删除元素创建树直至操作符栈为空
//            String str = opeStack.pop();
//            operand1 = getOperand(numStack);
//            operand2 = getOperand(numStack);
//            LinkedBinaryTree tree = new LinkedBinaryTree(str,operand2,operand1);
//            numStack.push(tree);
//        }
//    }
//
//    public ArrayUnorderedList getExpression(){
//        return numStack.pop().postOrder();
//    }
//
//    @Override
//    public String toString() {
//        String result = "";
//        Iterator tree = numStack.pop().iteratorPostOrder();
//        while (tree.hasNext()){
//            result += tree.next() + " ";
//        }
//        return result;
//    }

    public static String  toSuffix(String infix) {
        String result = "";
        //将字符串转换为数组
        String[] array = infix.split("\\s+");
        //存放操作数
        Stack<LinkedBinaryTree> num = new Stack();
        //存放操作符
        Stack<LinkedBinaryTree> op = new Stack();

        for (int a = 0; a < array.length; a++) {
            //如果是操作数，开始循环
            if (array[a].equals("+") || array[a].equals("-") || array[a].equals("*") || array[a].equals("/")) {
                if (op.empty()) {
                    //如果栈是空的，将数组中的元素建立新树结点并压入操作符栈
                    op.push(new LinkedBinaryTree<>(array[a]));
                } else {
                    //如果栈顶元素为+或-且数组的元素为*或/时，将元素建立新树结点并压入操作符栈
                    if ((op.peek().root.element).equals("+") || (op.peek().root.element).equals("-") && array[a].equals("*") || array[a].equals("/")) {
                        op.push(new LinkedBinaryTree(array[a]));
                    } else {
                        //将操作数栈中的两个元素作为左右孩子，操作符栈中的元素作为根建立新树
                        LinkedBinaryTree right = num.pop();
                        LinkedBinaryTree left = num.pop();
                        LinkedBinaryTree temp = new LinkedBinaryTree(op.pop().root.element, left, right);
                        //将树压入操作数栈，并将数组中的元素建立新树结点并压入操作符栈
                        num.push(temp);
                        op.push(new LinkedBinaryTree(array[a]));
                    }
                }

            } else {
                //将数组元素建立新树结点并压入操作数栈
                num.push(new LinkedBinaryTree<>(array[a]));
            }
        }
        while (!op.empty()) {
            LinkedBinaryTree right = num.pop();
            LinkedBinaryTree left = num.pop();
            LinkedBinaryTree temp = new LinkedBinaryTree(op.pop().root.element, left, right);
            num.push(temp);
        }
        //输出后缀表达式
        Iterator itr=num.pop().iteratorPostOrder();
        while (itr.hasNext()){
            result+=itr.next()+" ";
        }
        return result;
    }
}
