package Experiment2;

import New_week7.LinkedBinaryTree;

public class PITTree {
    public static void main(String[] args) {
        String[] preOrder = new String[]{"A","B","D","H","I","E","J","M","N","C","F","G","K","L"};
        String[] inOrder = new String[]{"H","D","I","B","E","M","J","N","A","F","C","K","G","L"};

        LinkedBinaryTree2 tree = new LinkedBinaryTree2();
        tree.initTree(preOrder,inOrder);
        System.out.println(tree.toString());
    }
}
