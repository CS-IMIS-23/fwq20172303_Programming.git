package Experiment2;

import New_week7.BinaryTreeNode;
import New_week7.LinkedBinaryTree;

public class LBTTest {
    public static void main(String[] args) {
        //构建结点
        BinaryTreeNode node1 = new BinaryTreeNode(22);
        BinaryTreeNode node2 = new BinaryTreeNode(45);
        BinaryTreeNode node3 = new BinaryTreeNode(123);
        BinaryTreeNode node4 = new BinaryTreeNode(5);
        BinaryTreeNode node5 = new BinaryTreeNode(65);
        BinaryTreeNode node6 = new BinaryTreeNode(99);
        BinaryTreeNode node7 = new BinaryTreeNode(345);
        BinaryTreeNode node8 = new BinaryTreeNode(23);
        BinaryTreeNode node9 = new BinaryTreeNode(9);

        //构建二叉树
        LinkedBinaryTree tree0 = new LinkedBinaryTree(node8.getElement());
        LinkedBinaryTree tree1 = new LinkedBinaryTree(node1.getElement());
        LinkedBinaryTree tree2 = new LinkedBinaryTree(node2.getElement());
        LinkedBinaryTree tree3 = new LinkedBinaryTree(node3.getElement());
        LinkedBinaryTree tree = new LinkedBinaryTree(node7.getElement());
        LinkedBinaryTree tree4 = new LinkedBinaryTree(node4.getElement(),tree1,tree2);
        LinkedBinaryTree tree5 = new LinkedBinaryTree(node5.getElement(),tree4,tree);
        LinkedBinaryTree tree6 = new LinkedBinaryTree(node9.getElement(),tree0,tree3);
        LinkedBinaryTree tree7 = new LinkedBinaryTree(node6.getElement(),tree5,tree6);

        //测试
        System.out.println("测试toString(): ");
        System.out.println(tree7.toString());
        System.out.println("测试getRight(): ");
        System.out.println(tree7.getRight());
        System.out.print("测试contains(): ");
        System.out.println(tree7.contains(1));
        System.out.print("测试preOrder(): ");
        System.out.println(tree7.preOrder());
        System.out.print("测试postOrder(): ");
        System.out.println(tree7.postOrder());
    }
}
