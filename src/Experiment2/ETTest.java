package Experiment2;

import New_week7.PostfixEvaluator;

import java.util.Scanner;

import static Experiment2.ExpressionTTree.toSuffix;

public class ETTest {
    public static void main(String[] args) {
            String infix, again;
            int result;
            Scanner in = new Scanner(System.in);

            do {
                PostfixEvaluator evaluator = new PostfixEvaluator();

                System.out.println("Enter a valid infix expression one token " + "at a time with a space between each token(e.g 1 + 2 * 3)");
                System.out.println("Each token must be an integer or an operator (+,-,*,/)");
                infix = in.nextLine();

                //中缀转后缀
                String suffix =toSuffix(infix);

                //输出结果
                result = evaluator.evaluate(suffix);

                System.out.println("\n后缀表达式为：" + suffix);
                System.out.println("计算结果为："+result);

                System.out.println("表达树为：:");
                System.out.println(evaluator.getTree());

                System.out.println("Evaluate another expression [Y/N]?");
                again = in.nextLine();
                System.out.println();
            }
            while (again.equalsIgnoreCase("Y"));

        }
    }