package cn.edu.besti.cs1723.FWQ2303;

import Experiment3.Point1.Searching;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SearhingTest {

    @Test
    public void testlinearSearch() throws Exception{
        // 正常测试
        Comparable list1[] = {1,23,456,77,2303};
        // 元素在所查找的范围内
        assertEquals(true, Searching.linearSearch(list1,0,3,77));
        assertEquals(true,Searching.linearSearch(list1,0,4,2303)); //边界测试
        // 元素不在所查找的范围内
        assertEquals(false,Searching.linearSearch(list1,1,3,1));

        Comparable list2[] = {"1","22","333","2303","5"};
        // 元素在所查找的范围内
        assertEquals(true,Searching.linearSearch(list2,0,3,"22"));
        assertEquals(true,Searching.linearSearch(list2,2,4,"2303"));
        // 元素不在所查找的范围内
        assertEquals(false,Searching.linearSearch(list2,1,3,"5"));

        Comparable list3[] = {"A","B","C","D","E"};
        // 元素在所查找的范围内
        assertEquals(true,Searching.linearSearch(list3,0,3,"B"));
        assertEquals(true,Searching.linearSearch(list3,0,4,"E")); //边界测试
        // 元素不在所查找的范围内
        assertEquals(false,Searching.linearSearch(list3,2,4,"A"));

//        // 异常测试1——声明错误
//        assertEquals(true,Searching.linearSearch(list1,1,2,2303));
//        // 异常测试2——空指针
//        Comparable list4[] = null;
//        assertEquals(false,Searching.linearSearch(list4,0,2,null));
    }
}