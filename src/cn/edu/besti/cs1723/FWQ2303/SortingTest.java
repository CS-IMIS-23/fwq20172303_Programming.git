package cn.edu.besti.cs1723.FWQ2303;

import Experiment3.Point1.Sorting;
import junit.framework.TestCase;
import org.junit.Test;

public class SortingTest extends TestCase{

    @Test
    public void testselectionSort() {
        // 正序测试
        Comparable list[] = {1,3,6,13,17,20,23,81,99,2303};
        Sorting.selectionSort(list);
        assertEquals(list[1],list[1]);

        // 逆序测试
        Comparable list2[] = {2303,99,81,23,20,17,13,6,3,1};
        Sorting.selectionSort(list2);
        assertEquals(list2[0],list[0]);

        // 乱序测试
        Comparable list3[] = {2303,1,20,17,23,3,6,13,99,81};
        Sorting.selectionSort(list3);
        assertEquals(list3[5],list[5]);

//        // 异常测试1——空指针异常
//        Comparable listerror[] = null;
//        Sorting.selectionSort(listerror);
//        assertEquals(listerror[0],list[0]);
//        // 异常测试2——越界异常
//        Comparable listerror1[] = {1,20,17,23,3,6,13,99,81,2303};
//        Sorting.selectionSort(listerror1);
//        assertEquals(listerror1[10],list[10]);
    }
}