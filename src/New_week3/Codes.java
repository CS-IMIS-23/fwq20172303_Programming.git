package New_week3;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Codes demonstrates the use of queues to encrypt and decrypt messages.
 */
public class Codes {
    /**
     *Encode and decode a message using a key of values stored in a queue.
     */
    public static void main(String[] args) {
        int[] key = {5,12,-3,8,-9,4,10};
        Integer KeyValue;
        String encoded = "",decoded = "";
        String message = "All programmers are playwrights and all" +
                "computers are lousy actors.";
        Queue<Integer> encodingQueue = new LinkedList<Integer>();
        Queue<Integer> decodingQueue = new LinkedList<Integer>();

        /** load key queue*/
        for (int scan = 0;scan < key.length;scan++){
            encodingQueue.add(key[scan]);
            decodingQueue.add(key[scan]);
        }

        /** encode message */
        for (int scan = 0;scan < message.length();scan++){
            KeyValue = encodingQueue.remove();
            encoded += (char)(message.charAt(scan) + KeyValue);
            encodingQueue.add(KeyValue);
        }
        System.out.println("Encoded Message: \n" + encoded + "\n");

        /** decode message */
        for (int scan = 0;scan < encoded.length();scan++){
            KeyValue = decodingQueue.remove();
            decoded += (char)(encoded.charAt(scan) - KeyValue);
            decodingQueue.add(KeyValue);
        }
        System.out.println("Decode Message: \n" + decoded);
    }
}
