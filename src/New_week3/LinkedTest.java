package New_week3;

public class LinkedTest {
    public static void main(String[] args) {
        LinkedQueue linkedQueue = new LinkedQueue();

        linkedQueue.enqueue("a");
        linkedQueue.enqueue("b");
        linkedQueue.enqueue("c");

        System.out.println(linkedQueue.first());
        System.out.println(linkedQueue.isEmpty());
        System.out.println(linkedQueue.size());
        System.out.println(linkedQueue.toString());
        linkedQueue.dequeue();
        System.out.println(linkedQueue.toString());
    }
}
