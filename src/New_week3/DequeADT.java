package New_week3;

public interface DequeADT<T> {
    //在队首添加元素
    public void addFirst(T element);
    //在队首删除元素
    public T removeFirst();

    //在队尾添加元素
    public void addLast(T element);
    //在队尾删除元素
    public T removeLast();

    //获取队首元素
    public T getFirst();
    //获取队尾元素
    public T getLast();

    //获取队内元素个数
    public int size();

    //判断队内是否为空
    public boolean isEmpty();

    @Override
    public String toString();
}
