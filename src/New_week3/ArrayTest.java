package New_week3;

public class ArrayTest {
    public static void main(String[] args) {
        CircularArrayQueue queue = new CircularArrayQueue();
        queue.enqueue("e");
        queue.enqueue("f");
        queue.enqueue("g");

        System.out.println(queue.isEmpty());
        System.out.println(queue.first());
        System.out.println(queue.size());
        System.out.println(queue.toString());
        queue.dequeue();
        System.out.println(queue.toString());
    }
}
