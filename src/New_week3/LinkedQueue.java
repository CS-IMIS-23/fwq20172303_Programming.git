package New_week3;


import New_week2.EmptyCollectionException;
import New_week2.LinearNode;

public class LinkedQueue<T> implements QueueADT<T>
{
    private int count;
    private LinearNode<T> head, tail;

    /**
     * Creates an empty queue.
     */
    public LinkedQueue()
    {
        count = 0;
        head = tail = null;
    }


    @Override
    public void enqueue(T element)
    {
        LinearNode<T> node = new LinearNode<T>(element);

        if (isEmpty()) {
            head = node;
        } else {
            tail.setNext(node);
        }

        tail = node;
        count++;
    }


    @Override
    public T dequeue() throws EmptyCollectionException
    {
        if (isEmpty()) {
            throw new EmptyCollectionException("queue");
        }

        T result = head.getElement();
        head = head.getNext();
        count--;

        if (isEmpty()) {
            tail = null;
        }

        return result;
    }

    public void addMiddle(int index,T element){
        LinearNode<T> node = new LinearNode<>(element);
        LinearNode<T> current = head;
        int j = 0;
        while (current != null && j < index - 2){//使指针指向index-1的位置
            current = current.getNext();
            j ++;
        }
        node.setNext(current.getNext());
        current.setNext(node);
        count++;
    }

    public void addFirst(T element){
        LinearNode<T> node = new LinearNode<T>((T) element);
        if (head == null) {
            head = node;
            tail = node;
        }
        else {
            node.setNext(head);
            head = node;
        }
        count++;
    }

    public void Delete(int index){
        LinearNode<T> current = head;
        LinearNode<T> temp = head;
        if (index == 0){
            head = head.getNext();
        }
        else {
            for (int i = 0;i < index - 1;i++){
                temp = current;
                current = current.getNext();
            }
            temp.setNext(current.getNext());
        }
        count--;
    }


    @Override
    public T first() throws EmptyCollectionException
    {
        if (isEmpty()) {
            throw new EmptyCollectionException("queue");
        }
        T result = head.getElement();

        return result;
    }


    @Override
    public boolean isEmpty()
    {
        if (size() == 0) {
            return true;
        } else {
            return false;
        }
    }


    @Override
    public int size()
    {
        return count;
    }

    public void Sort(){
        LinearNode<T> node = head,current = null;
        if (head == null || head.getNext() == null){
            return;
        }
        while (node.getNext() != current){
            while (node.getNext() != current){
                if (Integer.parseInt(String.valueOf(node.getElement()))  > (Integer.parseInt(String.valueOf(node.getNext().getElement())))){
//                    LinearNode<T> temp = node;
//                    temp.setElement(node.getElement());
                    T temp = node.getElement();
                    node.setElement(node.getNext().getElement());
                    node.getNext().setElement(temp);
//                    node.getNext().setNext(temp.getElement());
                }
                node = node.getNext();
            }
            current = node;
            node = head;
            LinearNode<T> temp = head;
            String str = "";
            int i = 0;
            while (temp != null){
                str += temp.getElement() + " ";
                temp = temp.getNext();
                i++;
            }
            System.out.println(str);
            System.out.println("元素总数为： " + count);
        }
        return;
    }


    @Override
    public String toString()
    {
        LinearNode node = head;
        String result="";
        int a = count;
        while (a > 0) {
            result += node.getElement()+ " ";
            node = node.getNext();
            a--;
        }
        return result;
    }
}
