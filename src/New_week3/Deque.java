package New_week3;

import New_week2.EmptyCollectionException;
import New_week2.LinearNode;

public class Deque<T> implements DequeADT<T>{
    private LinearNode<T> head , tail, front,pre,temp;
    private int count;

    public Deque(){
        count = 0;
        head = tail = null;
    }
    @Override
    public void addFirst(T element) {
        LinearNode<T> node = new LinearNode<T>((T) element);
        if (head == null) {
            head = node;
            tail = node;
        }
        else {
            node.setNext(head);
            head = node;
        }
        count++;
    }

    @Override
    public T removeFirst() throws EmptyCollectionException {
        if (isEmpty()){
            throw new EmptyCollectionException("queue");
        }
        front = head;
        T result = (T) front.getElement();
        head = head.getNext();
        count--;

        if (isEmpty()) {
            tail = null;
        }

        return result;
    }

    @Override
    public void addLast(T element) {
        LinearNode node = new LinearNode(element);

        if (isEmpty()){
            head = node;
        }
        else {
            tail.setNext(node);
        }
        tail = node;
        count++;
    }

    @Override
    public T removeLast() throws EmptyCollectionException{
        if (isEmpty()){
            throw new EmptyCollectionException("queue");
        }
        front = tail;
        temp = head;

        T result = front.getElement();
        while (temp.getNext() != tail){
            pre = temp;
            temp = temp.getNext();
        }
        temp.setNext(null);
        count--;
        return result;
    }

    @Override
    public T getFirst() {
        if (isEmpty()) {
            throw new EmptyCollectionException("queue");
        }
        T result = head.getElement();

        return result;
    }

    @Override
    public T getLast() {
        if (isEmpty()){
            throw new EmptyCollectionException("queue");
        }
        T result = tail.getElement();

        return result;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public boolean isEmpty() {
        if (size() == 0){
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public String toString() {
        LinearNode<T> node = head;
        String result = "";
        while (node != null) {
        result += node.getElement() + " ";
        node = node.getNext();
        }
        return result;
    }
}
