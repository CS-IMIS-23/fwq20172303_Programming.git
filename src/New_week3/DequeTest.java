package New_week3;

public class DequeTest {
    public static void main(String[] args) {
        Deque deque = new Deque();

        deque.addFirst("a");
        deque.addFirst("b");
        deque.addFirst("c");
        deque.addLast("z");
        System.out.println(deque.toString());
        System.out.println(deque.isEmpty());
        System.out.println(deque.getFirst());
        System.out.println(deque.getLast());
        System.out.println(deque.size());
        deque.removeFirst();
        System.out.println(deque.toString());
        deque.removeLast();
        System.out.println(deque.toString());
    }
}
