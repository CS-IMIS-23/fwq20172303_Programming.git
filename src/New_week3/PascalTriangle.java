package New_week3;

import java.util.Scanner;

public class PascalTriangle {
    public static void main(String[] args) {
        int n;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Please enter the number of rows to display： ");
        n = scanner.nextInt();
        CircularArrayQueue queue = new CircularArrayQueue();

        queue.enqueue(0);
        queue.enqueue(1);
        for (int i = 0;i <= n + 1; i++){
            System.out.print(" ");
        }
        System.out.println(1);

        for (int i = 0; i < n - 1; i++){
            queue.enqueue(0);
            for(int m = n - i; m >= 0;m--){
                System.out.print(" ");
            }
            for (int j = 1;j <= queue.size() - 1;j++){
                int a = (int) queue.first();
                queue.dequeue();
                int b = (int) queue.first();
                int c = a + b;
                queue.enqueue(c);
                System.out.print(c + " ");
            }
            System.out.println();
        }
    }
}
