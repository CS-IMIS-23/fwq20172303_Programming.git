package New_week5;


import New_week2.LinearNode;
import New_week4.ElementNotFoundException;
import New_week4.LinkedList;
import New_week4.UnorderedListADT;

/**
 * LinkedUnorderedList represents a singly linked implementation of an 
 * unordered list.
 *
 * @author Lewis and Chase
 * @version 4.0
 */
public class LinkedUnorderedList2<Product> extends LinkedList<Product> implements UnorderedListADT<Product>
{
    /**
     * Creates an empty list.
     */

    public LinkedUnorderedList2()
    {
        super();
    }

    /**
     * Adds the specified element to the front of this list.
     *
     * @param element the element to be added to the list
	 */
    @Override
    public void addToFront(Product element)
    {
        LinearNode<Product> node = new LinearNode<Product>((Product) element);
        if (head == null) {
            head = node;
            tail = node;
        }
        else {
            node.setNext(head);
            head = node;
        }
        count++;
    }
	
	/**
     * Adds the specified element to the rear of this list.
     *
     * @param element the element to be added to the list
	 */
    @Override
    public void addToRear(Product element)
    {
        LinearNode node = new LinearNode(element);

        if (isEmpty()){
            head = node;
        }
        else {
            tail.setNext(node);
        }
        tail = node;
        count++;
    }
	
	
    /**
     * Adds the specified element to this list after the given target.
     *
     * @param  element the element to be added to this list
	 * @param  target the target element to be added after
	 * @throws ElementNotFoundException if the target is not found
	 */
    @Override
    public void addAfter(Product element, Product target)
    {
        LinearNode<Product> node = new LinearNode<>(element);
        LinearNode<Product> current = head;
        LinearNode<Product> temp = current.getNext();

        for (int i = 0;i < count - 1;i++){
            if (target.equals(current.getElement())) {
                current.setNext(node);
                node.setNext(temp);
                count++;
                return;
            }
            current = temp;
            temp = temp.getNext();
        }
    }


    public String find(int number, LinkedUnorderedList2<New_week5.Product> list){
        LinkedUnorderedList2<New_week5.Product> list2 = list;
        int a = list.size();
        for (int i = 0; i < a;i++){
            if (number == list2.first().getNumber()){
                New_week5.Product pro = list2.first();
                String str = pro.toString();
                return str;
            }
            else {
                list2.removeFirst();
            }
        }
        return "找不到商品编号。";
    }

    public void selectionSort() {
        LinearNode<New_week5.Product> node = (LinearNode<New_week5.Product>) head;
        LinearNode<New_week5.Product> temp1 = null,temp2 = null,temp3 = null;

        New_week5.Product min = new New_week5.Product(00,"",0,"");
        while (node.getNext() != null){
            temp1 = null;
            temp2 = temp3 = node;
            while (temp2.getNext() != null){
                if (temp2.getNext().getElement().getNumber() < temp3.getElement().getNumber()){
                    temp3 = temp1 = temp2.getNext();
                    min = temp2.getNext().getElement();
                }
                temp2 = temp2.getNext();
            }
            if (temp1 != null){
                temp1.setElement(node.getElement());
                node.setElement(min);
            }
            node = node.getNext();
        }
    }
}
