package New_week5;

import New_week4.LinkedUnorderedList;

public class ProductTest {
    public static void main(String[] args) {
        Product product1 = new Product(01,"五花肉",10,"2018年10月10日");
        Product product2 = new Product(02,"薯片",6,"2018年9月10日");
        Product product3 = new Product(03,"方便面",5,"2018年8月10日");
        Product product4 = new Product(04,"蛋黄酥",22,"2018年10月5日");
        Product product5 = new Product(05,"珍珠奶茶",13,"2018年10月11日");

        LinkedUnorderedList2 list = new LinkedUnorderedList2();
        list.addToFront(product1);
        list.addToFront(product2);
        list.addAfter(product4,product2);
        list.addToRear(product3);
        list.addToRear(product5);
        System.out.println(list.toString());
        System.out.println("排序后：");
        list.selectionSort();
        System.out.println(list.toString());
        list.remove(product3);
        System.out.println("删除3号商品后： ");
        System.out.println(list.toString());
        list.addAfter(product3,product2);
        System.out.println("插入3号商品后： ");
        System.out.println(list.toString());
        
        System.out.println("查找功能测试——查找编号为2的商品： ");
        System.out.println(list.find(02,list));
        System.out.println("查找功能测试——查找编号为0（编号不存在的情况）的商品： ");
        System.out.println(list.find(00,list));
    }
}
