package New_week5;

import New_week4.LinkedList;

public class Product extends LinkedUnorderedList2 implements Comparable<Product>{
    private int number;//商品序号
    private String name;//商品名称
    private int price;//商品价格
    private String date;//生产日期

    public Product(int number, String name, int price, String date){
        this.number = number;
        this.name = name;
        this.price = price;
        this.date = date;
    }

    public int getNumber(){
        return number;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public String getDate() {
        return date;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        String result = "商品编号： " + number + "\n商品名称： " + name + "\n商品价格(1个/元)： " + price + "\n生产日期" + date + "\n";
        return result;
    }

    @Override
    public int compareTo(Product pro) {
        if (number > pro.getNumber()){
            return 1;
        }
        if (number < pro.getNumber()){
            return -1;
        }
        if (number == pro.getNumber()){
            if (name.compareTo(pro.getName()) > 0){
                return 1;
            }
            if (name.compareTo(pro.getName()) < 0){
                return -1;
            }
            if (name.compareTo(pro.getName()) == 0){
                if (price > pro.getPrice()){
                    return 1;
                }
                if (price < pro.getPrice()){
                    return -1;
                }
                if (price == pro.getPrice()){
                    if (date.compareTo(pro.getDate()) > 0){
                        return 1;
                    }
                    if (date.compareTo(pro.getDate()) < 0){
                        return -1;
                    }
                }
            }
        }
        return 0;
    }
}
