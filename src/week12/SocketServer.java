package week12;

import java.io.*;
import java.lang.reflect.Array;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;
import java.util.StringTokenizer;

import week9.classtest.InsertionSort;

/**
 * Created by besti on 2018/6/9.
 */
public class SocketServer {
    public static void main(String[] args) throws IOException {
        //1.建立一个服务器Socket(ServerSocket)绑定指定端口
        ServerSocket serverSocket=new ServerSocket(8800);
        //2.使用accept()方法阻止等待监听，获得新连接
        Socket socket=serverSocket.accept();
        //3.获得输入流
        InputStream inputStream=socket.getInputStream();
        BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(inputStream,"UTF-8"));
        //获得输出流
        OutputStream outputStream=socket.getOutputStream();
        PrintWriter printWriter=new PrintWriter(outputStream);
        //4.读取用户输入信息
        String info=null;
        while(!((info = bufferedReader.readLine()) ==null)){
            System.out.println("用户传给我的需要排序的数字是：" + info);
            //            String info1 = new String(info.getBytes("GBK"),"utf-8");
            String str = info;
            String[] temp = str.split(" ");//以空格拆分字符串，将字符串转化成数组
            int[] result = new int[temp.length];
            for(int i=0;i<temp.length;i++)//将字符串数组转化成整型数组
            {
                result[i] = Integer.parseInt(temp[i]);
            }
            InsertionSort.insertionSort(result);

            for (int result1 : result) {
                System.out.print(result1 + " ");
            }
            String s = Arrays.toString(result);
//            String a  = result1 + " ";

//            String reply="welcome";
            printWriter.write("你传给我的数字排序后是 " + s);
            printWriter.flush();
        }
        //5.关闭资源
        printWriter.close();
        outputStream.close();
        bufferedReader.close();
        inputStream.close();
        socket.close();
        serverSocket.close();
    }
}
