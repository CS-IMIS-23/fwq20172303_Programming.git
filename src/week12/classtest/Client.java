package week12.classtest;

//使用老师给的方法时传给服务器之后对方显示的总是一堆乱码，上网查了一下发现一篇博客专门解决了这个问题
//在博客中使用了另外一种方法叫：OutputStream.writeUTF()
//博客地址：https://blog.csdn.net/findmyself_for_world/article/details/41542245

import javax.crypto.Cipher;
import java.io.*;
import java.net.Socket;
import java.security.Key;
import java.util.Scanner;
import Project.Arithmetic.InfixToSuffix;

public class Client {
    public static void main(String[] args) {
        InfixToSuffix infixToSuffix = new InfixToSuffix();
        DataInputStream in = null;
        DataOutputStream out = null;
        Scanner scanner = new Scanner(System.in);
        String str;
        try {
            Socket mysocket = new Socket("172.16.43.217", 1013);
            in = new DataInputStream(mysocket.getInputStream());
            out = new DataOutputStream(mysocket.getOutputStream());
            System.out.println("客户端已启动");
            FileInputStream f = new FileInputStream("key1.dat");
            ObjectInputStream b = new ObjectInputStream(f);
            Key key = (Key) b.readObject();
            Cipher cp = Cipher.getInstance("DESede");
            cp.init(Cipher.ENCRYPT_MODE, key);
            System.out.println("请输入计算式：（以空格隔开每个运算符和运算数）");
            str = scanner.nextLine();
            infixToSuffix.conversion(str);
            String str1 = infixToSuffix.getMessage();
            byte[] ptext = str1.getBytes("UTF-8");
            byte[] ctext = cp.doFinal(ptext);
            System.out.println("我传送给服务器的信息为：");
            for (int i = 0; i < ctext.length; i++) {
                System.out.print(ctext[i] + ",");
            }
            System.out.println("");
            out.writeUTF(ctext.length + "");
            for (int i = 0; i < ctext.length; i++) {
                out.writeUTF(ctext[i] + "");
            }
            String s = in.readUTF();
            System.out.println("接收服务器的信息为：" + s);
        } catch (Exception e) {
            System.out.println("产生错误：" + e);
        }
    }
}
