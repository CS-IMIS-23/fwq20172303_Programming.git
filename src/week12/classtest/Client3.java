package week12.classtest;

//使用老师给的方法时传给服务器之后对方显示的总是一堆乱码，上网查了一下发现一篇博客专门解决了这个问题
//在博客中使用了另外一种方法叫：OutputStream.writeUTF()
//博客地址：https://blog.csdn.net/findmyself_for_world/article/details/41542245

import Project.Arithmetic.InfixToSuffix;
import week10.ExperimentalLesson.DH.KeyAgree;
import week10.ExperimentalLesson.DH.Key_DH;
import week10.ExperimentalLesson.DigestPass;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.net.Socket;
import java.security.Key;
import java.util.Scanner;

public class Client3 {
    public static void main(String[] args) {
        InfixToSuffix infixToSuffix = new InfixToSuffix();
        DataInputStream in = null;
        DataOutputStream out = null;
        Scanner scanner = new Scanner(System.in);
        String str;
        try {
            Socket mysocket = new Socket("172.16.43.217", 1013);
            in = new DataInputStream(mysocket.getInputStream());
            out = new DataOutputStream(mysocket.getOutputStream());
            System.out.println("客户端已启动");
            FileInputStream f = new FileInputStream("key1.dat");
            ObjectInputStream b = new ObjectInputStream(f);
            //生成客户端的私钥和公钥
            Key_DH.DH("Apub.dat","Apri.dat");
            FileInputStream fp = new FileInputStream("Apub.dat");
            ObjectInputStream bp = new ObjectInputStream(fp);
            Key key = (Key) bp.readObject();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            objectOutputStream.writeObject(key);
            byte[] kb = byteArrayOutputStream.toByteArray();
            out.writeUTF(kb.length + "");
            for (int i = 0; i < kb.length; i++) {
                out.writeUTF(kb[i] + "");
            }
            Thread.sleep(1000);//交出线程占用CPU时间一秒钟.
            int len = Integer.parseInt(in.readUTF());
            byte[] np = new byte[len];
            for (int i = 0;i<len;i++) {
                String temp = in.readUTF();
                np[i] = Byte.parseByte(temp);
            }
            ObjectInputStream objectInputStream = new ObjectInputStream (new ByteArrayInputStream (np));
            Key k2 = (Key)objectInputStream.readObject();;
            FileOutputStream f2 = new FileOutputStream("Bpub.dat");
            ObjectOutputStream b2 = new ObjectOutputStream(f2);
            b2.writeObject(k2);
            KeyAgree.DH("Bpub.dat","Apri.dat");
            FileInputStream f3 = new FileInputStream("sb.dat");
            byte[] keysb = new byte[24];
            f3.read(keysb);
            System.out.println("产生的公共密钥为：");
            for (int i = 0;i<24;i++) {
                System.out.print(keysb[i]+",");
            }
            System.out.println("");
            //使用DES对计算式进行加密
            SecretKeySpec secretKeySpec = new SecretKeySpec(keysb, "DESede");
            Cipher cp = Cipher.getInstance("DESede");
            cp.init(Cipher.ENCRYPT_MODE, secretKeySpec);
            System.out.println("请输入计算式：（以空格隔开每个运算符和运算数）");
            str = scanner.nextLine();
            infixToSuffix.conversion(str);
            String str1 = infixToSuffix.getMessage();
            //产生明文的MD5值
            String MD5 = DigestPass.DP(str1);
            byte[] ptext = str1.getBytes("UTF-8");
            byte[] ctext = cp.doFinal(ptext);
            System.out.println("明文的MD5值为： " );
            System.out.println(MD5);
            System.out.println("我传送给服务器的信息为：");
            for (int i = 0; i < ctext.length; i++) {
                System.out.print(ctext[i] + ",");
            }
            System.out.println("");
            out.writeUTF(ctext.length + "");
            for (int i = 0; i < ctext.length; i++) {
                out.writeUTF(ctext[i] + "");
            }
            out.writeUTF(MD5);
            //读取服务器传过来的信息
            String s = in.readUTF();
            System.out.println("接收服务器的信息为：" + s);
        } catch (Exception e) {
            System.out.println("产生错误：" + e);
        }
    }
}
