package week10.PP1303;

public class SelectionSortTest {
    public static void main(String[] args) {
        SelectionSortList rack = new SelectionSortList();

        rack.add(4);
        rack.add(3);
        rack.add(89);
        rack.add(7);
        rack.add(489);
        rack.add(29);
        rack.add(100);
        rack.selectionsort();

        System.out.println(rack);
    }
}
