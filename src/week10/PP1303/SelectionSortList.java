package week10.PP1303;


public class SelectionSortList {
    private SSNode list;

    public SelectionSortList(){
        list = null;
    }

    private class SSNode{
        int num;
        public SSNode next;
        public SSNode(int num1){
            num = num1;
            next = null;
        }
    }

    public void add(int num){
        SSNode node = new SSNode(num);
        SSNode current;

        if (list==null)
            list = node;
        else {
            current = list;
            while (current.next != null)
                current = current.next;
            current.next = node;
        }
    }

    public void selectionsort(){
        SSNode min;
        SSNode current;
        int temp;
        SSNode pre = list;
        while (pre != null){
            min = pre;
            current = min.next;
            while (current != null){
                if (current.num >= min.num){
                    current = current.next;
                }
                else if (current.num < min.num){
                    min = current;
                    current = current.next;
                }
            }
            temp = min.num;
            min.num = pre.num;
            pre.num = temp;
            pre = pre.next;
        }
    }

    @Override
    public String toString() {
        String result = "";
        SSNode current = list;
        while (current != null){
            result += current.num + " ";
            current = current.next;
        }
        return result;
    }
}
