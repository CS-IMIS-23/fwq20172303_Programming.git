package week10.PP1301;

public class DVDCollection {
    public static class DVDList{
        private DVDNode list;

        public DVDList(){
            list = null;
        }

        private class DVDNode{
            public DVD dvd;
            public DVDNode next;

            public DVDNode(DVD dvd1){
                dvd = dvd1;
                next = null;
            }
        }

        public void add(DVD a){
            DVDNode node = new DVDNode(a);
            DVDNode current;

            if (list == null)
                list = node;
            else {
                current = list;
                while (current.next != null)
                    current = current.next;
                current.next = node;
            }
        }

        @Override
        public String toString() {
            String result = "";

            DVDNode current = list;

            while (current != null){
                result += current.dvd + "\n";
                current = current.next;
            }
            return result;
        }
    }
}
