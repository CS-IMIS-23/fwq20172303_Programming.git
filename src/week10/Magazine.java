package week10;
//********************************************************************
//  Magazine.java       Author: Lewis/Loftus
//
//  Represents a single magazine.
//********************************************************************

public class Magazine 
{
   private String title;

   //-----------------------------------------------------------------
   //  Sets up the new magazine with its title.
   //-----------------------------------------------------------------
   public Magazine(String newTitle)
   {    
      title = newTitle;
   }

   //-----------------------------------------------------------------
   //  Returns this magazine as a string.
   //-----------------------------------------------------------------
   public String toString()
   {
      return title;
   }
   //修改了equals方法
    @Override
   public boolean equals(Object obj)
   {
   Magazine obj1 = (Magazine) obj;
   if(this.title.equals(obj1.title))
      return true;
   else
      return false;
   }
}
