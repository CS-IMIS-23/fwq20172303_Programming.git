package week10.ExperimentalLesson;

public class Complex //  定义构造函数。
{
    public double RealPart;    //  实数
    public double ImagePart;   //  虚数
    public double getRealPart()
    {
        return RealPart;
    }
    public double getImagePart()
    {
        return ImagePart;
    }
    public void setRealPart(double Real)
    {
        RealPart = Real;
    }
    public void setImagePart(double Image)
    {
        ImagePart = Image;
    }
    public Complex() {}
    public Complex(double R,double I){
        RealPart=R;
        ImagePart=I;
    }
    @Override   //   Override Object
    public boolean equals(Object obj)
    {
        if(this==obj) {
            return true;
        } else if(obj==null) {
            return false;
        } else
        {
            double R = ((Complex) obj).getRealPart();
            double I = ((Complex) obj).getImagePart();
            if((R==RealPart)&&(I==ImagePart))
            {
                return true;
            }
            else {
                return false;
            }
        }
    }
    @Override
    public String toString()
    {
        return RealPart+"+"+ImagePart+'i';
    }
    public Complex ComplexAdd(Complex a)  //  加法
    {
        Complex com = new Complex();
        com.ImagePart=this.ImagePart+a.ImagePart;
        com.RealPart=this.RealPart+a.RealPart;
        return com;
    }
    public Complex ComplexSub(Complex b){  //  减法
        Complex com = new Complex();
        com.ImagePart=this.ImagePart-b.ImagePart;
        com.RealPart=this.RealPart-b.RealPart;
        return com;
    }
    public Complex ComplexMulti(Complex c){    //  乘法
        Complex com = new Complex();
        com.RealPart=this.RealPart*c.RealPart-this.ImagePart*c.ImagePart;
        com.ImagePart=this.RealPart*c.ImagePart+this.ImagePart*c.RealPart;
        return com;
    }
    public Complex ComplexDiv(Complex d){    //  除法
        Complex com = new Complex();
        Complex b=new Complex(d.RealPart,-d.ImagePart);
        System.out.println(b.toString());
        com.RealPart=(this.ComplexMulti(b).RealPart)/(d.RealPart*d.RealPart+d.ImagePart*d.ImagePart);
        com.ImagePart=(this.ComplexMulti(b).ImagePart)/(d.RealPart*d.RealPart+d.ImagePart*d.ImagePart);
        System.out.println(com.ImagePart);
        return com;
    }
}
