package week10.ExperimentalLesson;

import junit.framework.TestCase;
import org.junit.Test;

import static org.junit.Assert.*;

public class ComplexTest extends TestCase{
    Complex x = new Complex(6,5);
    Complex y = new Complex(4,3);
    Complex z = new Complex(6,5);
    @Test
    public void testAdd(){
        assertEquals(10.0,x.ComplexAdd(y).RealPart);
        assertEquals(8.0,x.ComplexAdd(y).ImagePart);
    }

    public void testSub(){
        assertEquals(2.0,x.ComplexSub(y).RealPart);
        assertEquals(2.0,x.ComplexSub(y).ImagePart);
    }

    public void testMul(){
        assertEquals(9.0,x.ComplexMulti(y).RealPart);
        assertEquals(38.0,x.ComplexMulti(y).ImagePart);
    }

    public void testDiv(){
        assertEquals(1.56,x.ComplexDiv(y).RealPart);
        assertEquals(0.08,x.ComplexDiv(y).ImagePart);
    }
}