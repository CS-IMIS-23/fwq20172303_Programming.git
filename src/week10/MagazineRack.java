package week10;//*******************************************************************
//  MagazineRackC.java       Author: Lewis/Loftus
//
//  Driver to exercise the MagazineListC collection.
//*******************************************************************

public class MagazineRack
{
   //----------------------------------------------------------------
   //  Creates a MagazineListC object, adds several magazines to the
   //  list, then prints it.
   //----------------------------------------------------------------
   public static void main(String[] args)
   {    
      MagazineList rack = new MagazineList();
      
      rack.add(new Magazine("Time"));
      rack.add(new Magazine("Woodworking Today"));
      rack.add(new Magazine("Communications of the ACM"));
      rack.add(new Magazine("House and Garden"));
      rack.add(new Magazine("GQ"));
      rack.delete(new Magazine("GQ"));
      rack.insert(1,new Magazine("Brave New World"));

      System.out.println(rack); 
   }
}
