package week10;
//*******************************************************************
//  MagazineListC.java       Author: Lewis/Loftus
//
//  Represents a collection of magazines.
//*******************************************************************

public class MagazineList
{
   private MagazineNode list;

   //----------------------------------------------------------------
   //  Sets up an initially empty list of magazines.
   //----------------------------------------------------------------
   public MagazineList()
   {
      list = null;
   }

   //----------------------------------------------------------------
   //  Creates a new MagazineNode object and adds it to the end of
   //  the linked list.
   //----------------------------------------------------------------
   public void add(Magazine mag)
   {
      MagazineNode node = new MagazineNode(mag);
      MagazineNode current;

      if (list == null)
         list = node;
      else
      {
         current = list;
         while (current.next != null)
            current = current.next;
         current.next = node;
      }
   }
   //实现删除指定节点，本来想使用public void delete(int index)通过直接删除指定位置的元素来实现
   // 但是老师要求了要用public void delete(Magazine delNode)
   // 所以先修改了equals的方法使它能判断指定节点与实际是否相同
    public void delete(Magazine delNode)
    {
        MagazineNode pre = list;
        MagazineNode current = list;
        while (!current.magazine.equals(delNode) && current.next != null){
            current = current.next;
            pre = current;
        }
        if (!current.magazine.equals(delNode) && current.next == null)//找不到要删除的节点
        {
            System.out.println("Error! We can't find the node.");
        }
        else if (current.magazine.equals(delNode) && current == list)//在开头删除
        {
            list = list.next;
        }
        else if (current.magazine.equals(delNode) && current.next == null)//在结尾删除

            pre.next = null;

        else if (current.magazine.equals(delNode))//在中间删除
        {
            pre.next = current.next;
        }
    }

    public void insert(int index, Magazine newMagazine){
       MagazineNode current = list;
       int j = 0;
       while (current != null && j < index - 2){//使指针指向index-1的位置
           current = current.next;
           j ++;
       }
       MagazineNode node = new MagazineNode(newMagazine);
       node.next = current.next;
       current.next = node;
       //当index小于2时会发生错误，所以当index小于2时将前两个元素交换
       if (index < 2){
           Magazine temp = list.magazine;
           list.magazine = node.magazine;
           node.magazine = temp;
       }
    }

   //----------------------------------------------------------------
   //  Returns this list of magazines as a string.
   //----------------------------------------------------------------
   public String toString()
   {
      String result = "";

      MagazineNode current = list;

      while (current != null)
      {
         result += current.magazine + "\n";
         current = current.next;
      }

      return result;
   }

   //*****************************************************************
   //  An inner class that represents a node in the magazine list.
   //  The public variables are accessed by the MagazineListC class.
   //*****************************************************************
   private class MagazineNode
   {
      public Magazine magazine;
      public MagazineNode next;

      //--------------------------------------------------------------
      //  Sets up the node
      //--------------------------------------------------------------
      public MagazineNode(Magazine mag)
      {
         magazine = mag;
         next = null;
      }
   }
}
