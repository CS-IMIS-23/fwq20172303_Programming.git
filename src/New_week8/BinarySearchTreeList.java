package New_week8;


import New_week4.ListADT;
import New_week4.OrderedListADT;

import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

/**
 * BinarySearchTreeList represents an ordered list implemented using a binary
 * search tree.
 * 
 * @author Lewis and Chase
 * @version 4.0
 */
public class BinarySearchTreeList<T> extends LinkedBinarySearchTree<T> implements ListADT<T>, OrderedListADT<T>, Iterable<T>
{
    /**
     * Creates an empty BinarySearchTreeList.
     */
    public BinarySearchTreeList() 
    {
        super();
    }

    /**
     * Adds the given element to this list.
     * 
     * @param element the element to be added to the list
     */
    @Override
    public void add(T element)
    {
        addElement(element);
    }
    
    /**
     * Removes and returns the first element from this list.
     *
     * @return the first element in the list
     */
    @Override
    public T removeFirst()
    {
        return removeMin();
    }
    
    /**
     * Removes and returns the last element from this list.
     *
     * @return the last element from the list
     */
    @Override
    public T removeLast()
    {
        return removeMax();
    }

   /**
    * Removes and returns the specified element from this list.
    *
    * @param element the element being sought in the list
    * @return the element from the list that matches the target
    */
    @Override
    public T remove(T element)
    {
        return removeElement(element);
    }

   /**
    * Returns a reference to the first element on this list.
    *
    * @return a reference to the first element in the list
    */
    @Override
    public T first()
    {
        return findMin();
    }

   /**
    * Returns a reference to the last element on this list.
    *
    * @return a reference to the last element in the list
    */
    @Override
    public T last()
    {
        return findMax();
    }

    /**
    * Returns an iterator for the list.
    *
    * @return an iterator over the elements in the list
    */
    @Override
    public Iterator<T> iterator()
    {
        return iteratorInOrder();
    }

}

