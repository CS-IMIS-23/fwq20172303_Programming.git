package New_week8;

public class PP1103 {
    public static void main(String[] args) {
        LinkedBinarySearchTree<Integer> tree = new LinkedBinarySearchTree<Integer>(60);
        tree.addElement(50);
        tree.addElement(30);
        tree.addElement(20);
        tree.addElement(40);
        tree.addElement(70);
        tree.addElement(65);
        tree.addElement(80);
        tree.addElement(90);

        System.out.println(tree.toString());
        System.out.println("树中的最大值为： " + tree.findMax());
        tree.removeMax();
        System.out.println("删除原先的最大值后树中的最大值为： " + tree.findMax());
        System.out.println("树中的最小值为： " + tree.findMin());
        tree.removeMin();
        System.out.println("删除原先的最小值后树中的最小值为： " + tree.findMin());
    }
}
