package New_week8;

public class PP1108 {
    public static void main(String[] args) {
        AVLTree tree = new AVLTree();
        tree.insert(56);
        tree.insert(12);
        tree.insert(34);
        tree.insert(55);
        tree.insert(888);
        tree.insert(567);
        tree.insert(78);
        tree.insert(1);

        tree.print();
        tree.remove(1);
        tree.print();
    }
}
