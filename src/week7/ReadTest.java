package week7;

public class ReadTest {
    public static void main(String[] args) {
        Novel a = new Novel();
        Magazine b = new Magazine();
        Academic c = new Academic();

        a.setPages(336);
        a.setBookname("Stroies of Your Life and Others");
        a.setAuthor("Ted Jiang");
        a.setPublishinghouse("译林出版社");
        a.setKey("Aline, Language, Future");
        System.out.println(a);
        System.out.println();

        b.setPages(45);
        b.setKey("Popularization of Science");
        b.setIssue(8);
        b.setName("We Love Science");
        b.setType("Book for primary school children");
        System.out.println(b);
        System.out.println();

        c.setPages(3);
        c.setKey("Computer Science");
        c.setAname("假装这是一篇关于计算机科学与技术的学术期刊的标题");
        c.setAuthor("Lu Lala");
        c.setAspect("CS");
        c.setClassification(2);
        System.out.println(c);
    }
}
