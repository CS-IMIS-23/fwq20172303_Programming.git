package week7;

public class ReadMaterial {
    protected int pages;
    protected String key;

    public void setPages(int numPages){
        pages = numPages;
    }
    public int getPages(){
        return pages;
    }

    public void setKey(String bookkey){
        key = bookkey;
    }
    public String getKey() {
        return key;
    }
}
