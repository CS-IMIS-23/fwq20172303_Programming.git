package week7;

import week5.Coin;

import java.util.*;

public class MCTest {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String another = "y";
        int b=0, c=0;

        while (another.equalsIgnoreCase("y")){
            MonetaryCoin a = new MonetaryCoin();
            b = a.facevalue();
            System.out.println("The coin's face value is: " + b);
            c += b;

            System.out.print("Do you want to clip again? (y/n)");
            another = scan.nextLine();
        }
        System.out.println("The sum is: " + c);
        System.out.println();

        Coin mycoin = new Coin();
        mycoin.flip();
        System.out.println("The coin's face is: " + mycoin);

        if(mycoin.isHeads())
            System.out.println("The face is head.");
        else
            System.out.println("The face is tail.");
    }
}
