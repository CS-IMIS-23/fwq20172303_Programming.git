package week7;

// Server Classes
abstract class Data {
    abstract public void DisplayValue();
}
class Integer extends  Data {
    int value;
    Integer() {
        value=100;
    }
    public void DisplayValue(){
        System.out.println (value);
    }
}
class Long extends Data{
    int value;
    Long() {
        value=20172303;
    }
    public void DisplayValue(){
        System.out.println(value);
    }
}
// Pattern Classes
abstract class Factory {
    abstract public Data CreateDataObject();
}
class IntFactory extends Factory {
    public Data CreateDataObject(){
        return new Integer();
    }
}
class LongFactory extends Factory {
    public Data CreateDataObject(){
        return new Long();
    }
}
//Client classes
class Document {
    Data pd;
    Document(Factory pf){
        pd = pf.CreateDataObject();
    }
    public void DisplayData(){
        pd.DisplayValue();
    }
}
//Test class
public class MyDoc {
    static Document d, e;
    public static void main(String[] args) {
        d = new Document(new IntFactory());
        d.DisplayData();
        e = new Document(new LongFactory());
        e.DisplayData();
    }
}
