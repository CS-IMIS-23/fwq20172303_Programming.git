package week7;

public class Complex
{
    public double RealPart;
    public double ImagePart;

    public double getRealPart()
    {
        return RealPart;
    }
    public void setRealPart(double Real)
    {
        RealPart = Real;
    }

    public double getImagePart()
    {
        return ImagePart;
    }
    public void setImagePart(double Image)
    {
        ImagePart = Image;
    }

    public Complex() {}
    public Complex(double R,double I){
        RealPart=R;
        ImagePart=I;
    }

    public boolean equals(Object obj)
    {
        if(this==obj)
            return true;
        else if(obj==null)
            return false;
        else
        {
            double R = ((Complex) obj).getRealPart();
            double I = ((Complex) obj).getImagePart();
            if((R==RealPart)&&(I==ImagePart))
            {
                return true;
            }
            else return false;
        }
    }

    public String toString()
    {
        return RealPart+"+"+ImagePart+'i';
    }
    public Complex ComplexAdd(Complex a)
    {
        Complex com = new Complex();
        com.ImagePart=this.ImagePart+a.ImagePart;
        com.RealPart=this.RealPart+a.RealPart;
        return com;
    }
    public Complex ComplexSub(Complex b){
        Complex com = new Complex();
        com.ImagePart=this.ImagePart-b.ImagePart;
        com.RealPart=this.RealPart-b.RealPart;
        return com;
    }
    public Complex ComplexMulti(Complex c){
        Complex com = new Complex();
        com.RealPart=this.RealPart*c.RealPart-this.ImagePart*c.ImagePart;
        com.ImagePart=this.RealPart*c.ImagePart+this.ImagePart*c.RealPart;
        return com;
    }
    public Complex ComplexDiv(Complex d){
        Complex com = new Complex();
        Complex b=new Complex(d.RealPart,-d.ImagePart);
        System.out.println(b.toString());
        com.RealPart=(this.ComplexMulti(b).RealPart)/(d.RealPart*d.RealPart+d.ImagePart*d.ImagePart);
        com.ImagePart=(this.ComplexMulti(b).ImagePart)/(d.RealPart*d.RealPart+d.ImagePart*d.ImagePart);
        System.out.println(com.ImagePart);
        return com;
    }
}

