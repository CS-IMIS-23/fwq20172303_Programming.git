package week7;

import java.util.Random;

public class MonetaryCoin extends NewCoin{
    private int num;
    int[] value = {1, 2, 5, 10};

    public int facevalue(){
        int a;
        Random generator = new Random();
        a = generator.nextInt(4);

        num = value[a];
        return num;
    }

    public int getNum() {
        return num;
    }
}
