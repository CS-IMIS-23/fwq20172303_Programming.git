package week7;

import org.junit.Test;

import static org.junit.Assert.*;

public class StringBufferDemoTest {
    StringBuffer str1 = new StringBuffer("CaptainAmerica");//测试14个字符（小于16）
    StringBuffer str2 = new StringBuffer("CaptainAmericaCaptainAmerica");//测试28个字符（大于16小于34）
    StringBuffer str3 = new StringBuffer("CaptainAmericaCaptainAmericaCaptainAmerica");//测试42个字符（大于34）

    @Test
    public void testcharAt() throws Exception{
        assertEquals('C',str1.charAt(0));
        assertEquals('p',str1.charAt(2));
        assertEquals('r',str1.charAt(10));
    }
    @Test
    public void testcapacity() throws Exception{
        assertEquals(30,str1.capacity());
        assertEquals(44,str2.capacity());
        assertEquals(58,str3.capacity());
    }
    @Test
    public void testlength() throws Exception{
        assertEquals(14,str1.length());
        assertEquals(28,str2.length());
        assertEquals(42,str3.length());
    }
    @Test
    public void testindexOf() throws Exception{
        assertEquals(0,str1.indexOf("Cap"));
        assertEquals(2,str1.indexOf("pta"));
        assertEquals(10,str1.indexOf("ric"));
    }
}