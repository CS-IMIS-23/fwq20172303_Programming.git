package week7;

public class Novel extends ReadMaterial{
    private String bookname, author, publishinghouse;

    public void setBookname(String bk){
        bookname = bk;
    }

    public String getBookname() {
        return bookname;
    }

    public void setAuthor(String au){
        author = au;
    }

    public String getAuthor() {
        return author;
    }

    public void setPublishinghouse(String ph){
        publishinghouse = ph;
    }

    public String getPublishinghouse() {
        return publishinghouse;
    }

    public String toString() {
        String result;
        result = "Bookname: " + bookname + "\nAuthor: " + author + "\nPublishinghoues: " + publishinghouse +
                "\nPages: " + pages + "\nKey: " + key;
        return result;
    }
}
