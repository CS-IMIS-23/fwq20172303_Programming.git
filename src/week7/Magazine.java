package week7;

public class Magazine extends ReadMaterial{
    private int issue; //期刊号
    private String name, type;

    public void setIssue(int num){
        num = issue;
    }

    public int getIssue(){
        return issue;
    }

    public void setName(String nm){
        name = nm;
    }

    public String getName() {
        return name;
    }

    public void setType(String tp){
        type = tp;
    }

    public String getType() {
        return type;
    }

    public String toString() {
        String result;
        result = "Name： " + name + "\nIssue: " + issue + "\nType: " + type + "\nPages: " + pages + "\nKey: " + key;
        return result;
    }
}
