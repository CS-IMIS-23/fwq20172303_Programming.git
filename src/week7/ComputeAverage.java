package week7;

//This is a program to calculate a student's average score.

public class ComputeAverage {
    public static void main(String ... args) {
        double a=0, b;
        double c;
        for (String value : args)
            a += Integer.parseInt(value);
        b = args.length - 1;
        c = (a - Integer.parseInt(args[0]))/b;
        System.out.println(args[0] + "'s average score is : " + c);
    }
}