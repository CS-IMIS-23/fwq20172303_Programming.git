package week7;

public class StringBufferDemo {
   public static void main(String [] args){
       StringBuffer buffer = new StringBuffer();
       buffer.append('C');
       buffer.append("aptainAmerica");
       System.out.println(buffer.charAt(1));
       System.out.println(buffer.capacity());
       System.out.println(buffer.indexOf("tain"));
       System.out.println("buffer = " + buffer.toString());
       System.out.println(buffer.length());
       }
}
