package week7;

public class Academic extends ReadMaterial{
    private int classification; // 分级
    private String Aname, author, aspect;

    public void setClassification(int num1){
        classification = num1;
    }

    public int getClassification(){
        return classification;
    }

    public void setAname(String name){
        Aname = name;
    }

    public String getAname() {
        return Aname;
    }

    public void setAuthor(String au){
        author = au;
    }

    public String getAuthor() {
        return author;
    }

    public void setAspect(String ap){
        aspect = ap;
    }

    public String toString() {
        String result;
        result = "Name: " + Aname + "\nClassification: " + classification + "\nAuthor: " + author +
                "\nAspect: " + aspect + "\nPages: " + pages + "\nKey: " + key;
        return result;
    }
}
