package week6;

import java.lang.String;

public class AccountCollection {
    private int a;
    AccountBak.Account[] account;

    // 创建一个容量为30的数组来储存银行信息
    public AccountCollection(){
        account = new AccountBak.Account[30];
        a = 0;
    }

    // 建立账户
    public void setAccountnames(String name, long anumber, double balance){
        account[a] = new AccountBak.Account(name , anumber, balance);
        a++;
    }

    //判断用户是否存在并确认账号、用户名是否正确，当用户存在且正确时，允许存款
    public void savemoney(String name, long anumber,double balance){
        for (int b = 0; b < 30;b++)
            if (account[b] != null)
                if (account[b].getName().equals(name)&&account[b].getAcctNumber() == anumber)
                    account[b].deposit(balance);
    }

    //判断用户是否存在并确认账号、用户名是否正确，当用户存在且正确时，允许取款
    public void withdrawmoney(String name, long anumber, double balance){
        for (int b = 0; b < 30;b++)
            if (account[b] != null)
                if (account[b].getName().equals(name)&&account[b].getAcctNumber() == anumber)
                    if (account[b].getAcctNumber() > balance)
                        account[b].withdraw(balance);
    }

    //加息方法
    public void upInterst(){
        int i = 0;
        while (i < 30){
            if (account[i] != null)
            {
                account[i].addInterest();
            }
            i++;
        }
    }
}
