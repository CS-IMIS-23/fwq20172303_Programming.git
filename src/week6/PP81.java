package week6;

import java.util.Scanner;

public class PP81 {
    public static void main(String[] args) {
        final int LIMIT = 51;
        int[] num = new int[LIMIT];
        int a;

        Scanner scan = new Scanner(System.in);

        System.out.println("This is a program to calculate the times of the numbers you enter.");
        System.out.println("If you enter a number out of 50, the program will end.");
        System.out.println();

        System.out.println("Enter a integer within 0-50: ");
        a = scan.nextInt();
        num[a]++;

        while (a >= 0 && a <= 50){
            System.out.println("Enter a integer within 0-50: ");
            a = scan.nextInt();
            if (a > 50)
                break;
            num[a]++;
        }

        for (int b=0;b <=50;b++){
            System.out.println("The times of " + b + " is " + num[b]);
        }
    }
}
