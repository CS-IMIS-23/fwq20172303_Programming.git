package week6;
//******************************************************************************
// Primes.java
//
// Demonstrates the use of an initializer list for an array.
//******************************************************************************

public class Primes {
    //---------------------------------------------------------------------------------------------
    //  Stores some prime numbers in an array and prints them.
    //---------------------------------------------------------------------------------------------
    public static void main(String[] args) {
        int[] primeNums = { 3, 5, 7, 11, 13, 15, 17, 19};

        System.out.println("Array length: " + primeNums.length);

        System.out.println("The first few prime numbers are:");

        for (int prime : primeNums)
            System.out.print(prime + " ");
    }
}
