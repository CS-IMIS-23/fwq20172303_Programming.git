package week6;

public class BankTest {
    public static void main(String[] args) {
        AccountCollection account1 = new AccountCollection();

        //添加账户
        account1.setAccountnames("Tony Stark", 0001,0);
        account1.setAccountnames("Peter Parker", 0002,0);
        account1.setAccountnames("Steve Rogers",0003,0);
        account1.setAccountnames("Bruce Banner,who has 7 doctor's degree without money.",0004,0);
        System.out.println("Accounts of our bank: ");
        System.out.println(account1.account[0]);
        System.out.println(account1.account[1]);
        System.out.println(account1.account[2]);
        System.out.println(account1.account[3]);
        System.out.println();

        //存取款操作
        account1.savemoney("Peter Parker",0002,10000);
        account1.savemoney("Steve Rogers",0003,90000000);
        account1.savemoney("Tony Stark",0001,500000);
        account1.withdrawmoney("Tony Stark", 0001,10000);

        //加息之后
        account1.upInterst();

        System.out.println("After some trades: ");
        System.out.println(account1.account[0]);
        System.out.println(account1.account[1]);
        System.out.println(account1.account[2]);
        System.out.println(account1.account[3]);
    }
}
