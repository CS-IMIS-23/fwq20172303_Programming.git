package week6;

import java.util.Scanner;

public class PP85 {
    public static void main(String[] args) {
        int[] num = new int[50];
        String another = "y";

        Scanner scan = new Scanner(System.in);
        int a, b=0, n=0;

        while (another.equalsIgnoreCase("y")){
            System.out.print("Enter an integer: ");
            a = scan.nextInt();
            b += a;
            num[n] = a;
            n++;

            System.out.print("Do you want to enter another integer? (y/n)");
            another = scan.next();
        }

        double c;
        c = (double)b/n;
        System.out.println("The mean is: " + c);

        double d, f, g, h=0, i=0;
        for (int e=0;e < n;e++){
            f = num[e] - c;
            g = f * f;
            i += g;
        }
        h = Math.sqrt(i);
        System.out.println("The sd is: " + h);
    }
}
