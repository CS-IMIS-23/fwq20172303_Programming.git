package week6;

import java.util.Scanner;

public class Classtest {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n,b;

        System.out.println("The method of foor loop.");
        System.out.print("Enter the n: ");
        n = scan.nextInt();
        b = n;

        for (int i = 1;i < n;i++)
            b *= i;
        System.out.println("The n! is: " + b);

        System.out.println();

        System.out.println("The method of whlie loop.");
        int a, c, j = 1;
        System.out.print("Enter the n: ");
        a = scan.nextInt();
        c = a;

        while (j < a){
            c *= j;
            j++;
        }
        System.out.println("The n! is: " + c);
    }
}
