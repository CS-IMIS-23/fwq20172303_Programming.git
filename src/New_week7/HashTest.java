package New_week7;

import New_week6.Searching;

import java.util.Hashtable;

public class HashTest {
    public static void main(String[] args) {
        int[] data = {11,78,10,1,3,2,4,21,24};

        HashSort.hashTable(data,11);
        System.out.println("冲突次数为： " + HashSort.getCount());
        System.out.println("ASL = " + HashSort.getASL(9)); // (1*6+2*3)/9 = 12/9

        System.out.println("测试9是否在内： " + HashSort.HashSearch(data,9));
        System.out.println("测试24是否在内： "+ HashSort.HashSearch(data,24));

    }
}
