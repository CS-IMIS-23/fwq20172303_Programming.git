package New_week7;

public class PP101_105 {
    public static void main(String[] args) {
        //构建结点
        BinaryTreeNode node1 = new BinaryTreeNode(22);
        BinaryTreeNode node2 = new BinaryTreeNode(45);
        BinaryTreeNode node3 = new BinaryTreeNode(123);
        BinaryTreeNode node4 = new BinaryTreeNode(5);
        BinaryTreeNode node5 = new BinaryTreeNode(65);
        BinaryTreeNode node6 = new BinaryTreeNode(99);
        BinaryTreeNode node7 = new BinaryTreeNode(345);

        //构建二叉树
        LinkedBinaryTree tree1 = new LinkedBinaryTree(node1.getElement());
        LinkedBinaryTree tree2 = new LinkedBinaryTree(node2.getElement());
        LinkedBinaryTree tree3 = new LinkedBinaryTree(node3.getElement());
        LinkedBinaryTree tree = new LinkedBinaryTree(node7.getElement());
        LinkedBinaryTree tree4 = new LinkedBinaryTree(node4.getElement(),tree1,tree2);
        LinkedBinaryTree tree5 = new LinkedBinaryTree(node5.getElement(),tree4,tree3);
        LinkedBinaryTree tree6 = new LinkedBinaryTree(node6.getElement(),tree5,tree);

        System.out.println(tree6);
        tree6.removeRightSubtree();
        System.out.println("删除右子树后：");
        System.out.println(tree6);
        tree6.removeAllElements();
        System.out.println("删除所有元素后： ");
        System.out.println(tree6);
    }
}
