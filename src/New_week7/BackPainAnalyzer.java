package New_week7;

import java.io.*;

/**
 * BackPainAnaylyzer demonstrates the use of a binary decision tree to 
 * diagnose back pain.
 */
public class BackPainAnalyzer
{
    /**
     *  Asks questions of the user to diagnose a medical problem.
     */
    public static void main (String[] args) throws FileNotFoundException
    {
        System.out.println ("So, you're having back pain.");

        DecisionTree expert = new DecisionTree("input.txt");
        expert.evaluate();

        System.out.println("决策树的深度为： " + expert.getTree().getHeight());
        System.out.println("决策树的叶子数为： " + expert.getTree().CountLeaf(expert.getTree().getRootNode()));
    }
}
