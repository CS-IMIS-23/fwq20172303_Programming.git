package New_week7;

import New_week2.LinearNode;

public class HashSort {
    public static int count = 0; //计算冲突次数
    public static double time = 0;

    public static boolean HashSearch(int[] data, int key) {
        // 构建哈希表
        LinearNode[] hashtable = hashTable(data, 11);
        // 查找key是否在哈希表中
        int k = key % 11;
        LinearNode current = hashtable[k];
        while (current != null && (int)current.getElement() != key) {
            current = current.getNext();
//            count++;
        }
        if (current == null) {
            return false;
        } else {
            return true;
        }
    }

    //构造哈希表
    public static LinearNode[] hashTable(int[] data, int p){
        LinearNode[] hashtable = new LinearNode[p];
        int k; //k代表哈希函数计算的单元地址
        for (int i = 0; i < data.length; i++){
            LinearNode node = new LinearNode();
            node.setElement(data[i]);
            k = data[i] % p;
            if (hashtable[k] == null){
                hashtable[k] = node;
                time++;
            }else {
                LinearNode current = hashtable[k];
                while (current.getNext() != null){
                    current = current.getNext();
                }
                current.setNext(node);
                count++;
            }
        }
        return hashtable;
    }


    public static double getASL(int length){
        double ASL = (time + count*2) / length;
        return ASL;
    }

    public static int getCount(){
        return count;
    }
}
