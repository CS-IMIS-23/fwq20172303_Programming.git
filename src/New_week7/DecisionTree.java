package New_week7;


import sun.misc.Queue;

import java.util.*;
import java.io.*;

/**
 * The DecisionTree class uses the LinkedBinaryTree class to implement 
 * a binary decision tree. Tree elements are read from a given file and  
 * then the decision tree can be evaluated based on user input using the
 * evaluate method. 
 * 
 * @author Lewis and Chase
 * @version 4.0
 */
public class DecisionTree{
    public LinkedBinaryTree<String> tree;

    public LinkedBinaryTree getTree(){
        return tree;
    }
    /**
     * Builds the decision tree based on the contents of the given file
     *
     * @param filename the name of the input file
     * @throws FileNotFoundException if the input file is not found
     */
    public DecisionTree(String filename) throws FileNotFoundException
    {
        File inputFile = new File(filename);
        Scanner scan = new Scanner(inputFile);
        int numberNodes = scan.nextInt();
        scan.nextLine();
        int root = 0, left, right;
        
        List<LinkedBinaryTree<String>> nodes = new ArrayList<LinkedBinaryTree<String>>();
        for (int i = 0; i < numberNodes; i++) {
            nodes.add(i,new LinkedBinaryTree<String>(scan.nextLine()));
        }
        
        while (scan.hasNext())
        {
            root = scan.nextInt();
            left = scan.nextInt();
            right = scan.nextInt();
            scan.nextLine();
            
            nodes.set(root, new LinkedBinaryTree<String>((nodes.get(root)).getRootElement(), 
                                                       nodes.get(left), nodes.get(right)));
        }
        tree = nodes.get(root);
    }

    /**
     *  Follows the decision tree based on user responses.
     */
    public void evaluate()
    {
        LinkedBinaryTree<String> current = tree;
        Scanner scan = new Scanner(System.in);

        while (current.size() > 1)
        {
            System.out.println (current.getRootElement());
            if (scan.nextLine().equalsIgnoreCase("N")) {
                current = current.getLeft();
            } else {
                current = current.getRight();
            }
        }

        System.out.println (current.getRootElement());
    }

    //递归实现层序遍历
    public void levelOrder(BinaryTreeNode<String> node) {
        if (node == null) {
            return;
        }

        int depth = depth(node);

        for (int i = 1; i <= depth; i++) {
            levelOrder(node, i);
        }
    }

    private void levelOrder(BinaryTreeNode<String> node, int level) {
        if (node == null || level < 1) {
            return;
        }

        if (level == 1) {
            System.out.println(node.element);
            return;
        }

        // 左子树
        levelOrder(node.left, level - 1);
        // 右子树
        levelOrder(node.right, level - 1);
    }
    //计算深度
    public int depth(BinaryTreeNode<String> node) {
        if (node == null) {
            return 0;
        }

        int l = depth(node.left);
        int r = depth(node.right);
        if (l > r) {
            return l + 1;
        } else {
            return r + 1;
        }
    }

    //非递归实现层序遍历
    public void UnrecursionLevelOrder(){
       BinaryTreeNode root = tree.root;
        if (root == null){
            return;
        }

        BinaryTreeNode temp = null;
        LinkedList<BinaryTreeNode> list = new LinkedList<>();
        list.add(root);

        while (list.size() != 0){
            temp = list.remove();
            System.out.println(temp.element);

            if (temp.left != null){
                list.add(temp.left);
            }
            if (temp.right != null){
                list.add(temp.right);
            }
        }
    }
}