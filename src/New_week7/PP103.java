package New_week7;

public class PP103 {
    public static void main(String[] args) {
        //构建结点
        BinaryTreeNode node1 = new BinaryTreeNode(22);
        BinaryTreeNode node2 = new BinaryTreeNode(45);
        BinaryTreeNode node3 = new BinaryTreeNode(123);
        BinaryTreeNode node4 = new BinaryTreeNode(5);


        node2.setLeft(node4);
        node1.setLeft(node2);
        node1.setRight(node3);
        System.out.println(node1.isLeaf());
        System.out.println(node2.isLeaf());
        System.out.println(node3.isLeaf());
        System.out.println(node4.isLeaf());
    }
}
