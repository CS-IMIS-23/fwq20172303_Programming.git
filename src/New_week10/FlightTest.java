package New_week10;

public class FlightTest {
    public static void main(String[] args) {
        FlightNet net = new FlightNet();

        net.addCity("太原");
        net.addCity("北京");
        net.addCity("上海");
        net.addCity("潘多拉星球");
        net.addCity("伦敦");
        net.addCity("波士顿");

        net.addEdge("太原","北京",200);
        net.addEdge("上海","伦敦",3000);
        net.addEdge("太原","上海",400);
        net.addEdge("上海","北京",100);
        net.addEdge("北京","波士顿",1500);
        net.addEdge("伦敦","波士顿",300);


        net.getShortesPath("太原","波士顿");
        System.out.println();
        net.getShortesPath("太原","潘多拉星球");

        System.out.println();
        net.getShortesWeighPath("太原","伦敦");
    }
}
