package New_week10;

public class LinearGraphTest {
    public static void main(String[] args) {
        LinearGraph graph = new LinearGraph();

        graph.addVertex("A");
        graph.addVertex("B");
        graph.addVertex("C");
        graph.addVertex("D");
        graph.addVertex("E");
        graph.addVertex("F");
        graph.addVertex("H");
        graph.addEdge("A","B");
        graph.addEdge("B","E");
        graph.addEdge("C","D");
        graph.addEdge("E","H");
        graph.addEdge("A","H");
        graph.addEdge("F","A");

        graph.toString();
    }
}
