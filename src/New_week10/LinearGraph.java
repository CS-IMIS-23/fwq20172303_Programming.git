package New_week10;



import New_week2.LinkedStack;
import New_week2.StackADT;
import New_week3.LinkedQueue;
import New_week3.QueueADT;
import New_week4.ArrayUnorderedList;
import New_week4.ElementNotFoundException;
import New_week4.UnorderedListADT;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinearGraph implements GraphADT{
    private ArrayList<VerticeNode> vertices; // 用于存储顶点
    private int numVertices; // 记录顶点的个数
    private int modCount;
    private int a;

    public LinearGraph(){
        numVertices = 0;
        modCount = 0;
        a = 0;
        vertices = new ArrayList<VerticeNode>();
    }

    @Override
    public void addVertex(Object vertex) {
        VerticeNode node  = new VerticeNode(vertex);
        vertices.add(node);
        modCount++;
        numVertices++;
    }

    @Override
    public void removeVertex(Object vertex) {
        int i = 0;
        // 将目标顶点删除
        while (vertices.get(i).getElement() != vertex){
            i++;
        }
        vertices.remove(i);
        // 将顶点之后的列表里的元素前移
        for (int j = 0;j < numVertices;j++){
            VerticeNode temp = vertices.get(j);
            while (temp.getNext() != null){
                if (temp.getNext().getElement() == vertex){
                    temp.setNext(temp.getNext().getNext());
                }
                temp = temp.getNext();
            }
            break;
        }
        modCount--;
        numVertices--;
    }

    @Override
    public void addEdge(Object vertex1, Object vertex2) {
        // 寻找顶点要被添加的位置
        int i = 0;
        while (vertices.get(i).getElement() != vertex1){
            i++;
        }
        // 在顶点一的链表末端添加顶点二，在两顶点之间建立边
        VerticeNode temp = vertices.get(i);
        while (temp.getNext() != null){
            temp = temp.getNext();
        }
        temp.setNext(new VerticeNode(vertex2));

        // 对顶点二的链表进行上述相同操作
        int j = 0;
        while (vertices.get(j).getElement() != vertex2){
            j++;
        }
        VerticeNode temp1 = vertices.get(j);
        while (temp1.getNext() != null){
            temp1 = temp1.getNext();
        }
        temp1.setNext(new VerticeNode(vertex1));
    }

    @Override
    public void removeEdge(Object vertex1, Object vertex2) {
        // 找到所要删除的顶点的位置
        int i = 0;
        while (vertices.get(i).getElement() != vertex1){
            i++;
        }
        // 在顶点一的链表中找到顶点二的前驱结点
        VerticeNode temp = vertices.get(i);
        while (temp.getNext().getElement() != vertex2){
            temp = temp.getNext();
        }
        // 如果顶点二位于末端，直接将temp的next设为空即可
        if (temp.getNext().getNext() == null){
            temp.setNext(null);
        }
        // 如果顶点二位于中间，将temp的next设置为顶点二的next即可
        else {
            temp.getNext().setNext(temp.getNext().getNext());
        }
    }

    public int getIndex(Object vertex){
        int i = 0;
        while (i < numVertices){
            if (vertices.get(i).getElement() != vertex){
                i++;
            }
            else {
                break;
            }
        }
        return i;
    }

    @Override
    public Iterator iteratorBFS(Object startVertex) {
        return iteratorBFS(getIndex(startVertex));
    }

    public Iterator iteratorBFS(int startIndex){
        Integer x;
        QueueADT<Integer> traversalQueue = new LinkedQueue<Integer>();
        UnorderedListADT resultList = new ArrayUnorderedList();

        // 若所给索引值无效，抛出错误
        if (!indexIsValid(startIndex)){
            throw new ElementNotFoundException("Graph");
        }

        // 设置每个顶点是否被访问，初始设置为未访问
        boolean[] visited = new boolean[numVertices];
        for (int i = 0;i < numVertices;i++){
            visited[i] = false;
        }

        // 起始顶点进入队列，并标记为已访问
        traversalQueue.enqueue(startIndex);
        visited[startIndex] = true;

        // 开始循环，每次把队列中的首顶点添加到resultList中，将与首顶点连接的还未进入队列的顶点加入队列，并标记为已访问
        while (!traversalQueue.isEmpty()){
            x = traversalQueue.dequeue();
            resultList.addToRear(vertices.get(x).getElement());

            for (int i = 0;i < numVertices;i++){
                if (hasEdge(x,i) && !visited[i]){
                    traversalQueue.enqueue(i);
                    visited[i] = true;
                    a++;
                }
            }
        }
        return new GraphIterator(resultList.iterator());
    }

    // 判断两顶点之间是否有边
    public boolean hasEdge(int a,int b){
        if (a == b){
            return false;
        }
        VerticeNode vertex1 = vertices.get(a);
        VerticeNode vertex2 = vertices.get(b);
        while (vertex1 != null){
            if (vertex1.getElement() == vertex2.getElement()){
                return true;
            }
            vertex1 = vertex1.getNext();
        }
        return false;
    }
    @Override
    public Iterator iteratorDFS(Object startVertex) {
        return iteratorDFS(getIndex(startVertex));
    }

    // 深度优先遍历与广度优先遍历类似，主不过把队列改成了栈
    public Iterator iteratorDFS(int startIndex) {
        Integer x;
        boolean found;
        StackADT<Integer> traversalStack = new LinkedStack<Integer>();
        UnorderedListADT resultList = new ArrayUnorderedList();
        boolean[] visited = new boolean[numVertices];

        if (!indexIsValid(startIndex)) {
            return resultList.iterator();
        }

        for (int i = 0; i < numVertices; i++) {
            visited[i] = false;
        }

        traversalStack.push(new Integer(startIndex));
        resultList.addToRear(vertices.get(startIndex).getElement());
        visited[startIndex] = true;

        while (!traversalStack.isEmpty())
        {
            x = traversalStack.peek();
            found = false;

            //Find a vertex adjacent to x that has not been visited
            //     and push it on the stack
            for (int i = 0; (i < numVertices) && !found; i++)
            {
                if (hasEdge(x,i) && !visited[i])
                {
                    traversalStack.push(new Integer(i));
                    resultList.addToRear(vertices.get(startIndex).getElement());
                    visited[i] = true;
                    found = true;
                }
            }
            if (!found && !traversalStack.isEmpty()) {
                traversalStack.pop();
            }
        }
        return new GraphIterator(resultList.iterator());
    }
    @Override
    public Iterator iteratorShortestPath(Object startVertex, Object targetVertex) {
        return null;
    }

    public Iterator iteratorShortestPath(int startIndex, int targetIndex)
    {
        UnorderedListADT resultList = new ArrayUnorderedList();
        if (!indexIsValid(startIndex) || !indexIsValid(targetIndex)) {
            return resultList.iterator();
        }

        Iterator<Integer> it = iteratorShortestPathIndices(startIndex,
                targetIndex);
        while (it.hasNext()) {
            resultList.addToRear(vertices.get(it.next()).getElement());
        }
        return new GraphIterator(resultList.iterator());
    }

    protected Iterator<Integer> iteratorShortestPathIndices
            (int startIndex, int targetIndex)
    {
        int index = startIndex;
        int[] pathLength = new int[numVertices];
        int[] predecessor = new int[numVertices];
        QueueADT<Integer> traversalQueue = new LinkedQueue<Integer>();
        UnorderedListADT<Integer> resultList =
                new ArrayUnorderedList<Integer>();

        if (!indexIsValid(startIndex) || !indexIsValid(targetIndex) ||
                (startIndex == targetIndex)) {
            return resultList.iterator();
        }

        boolean[] visited = new boolean[numVertices];
        for (int i = 0; i < numVertices; i++) {
            visited[i] = false;
        }

        traversalQueue.enqueue(new Integer(startIndex));
        visited[startIndex] = true;
        pathLength[startIndex] = 0;
        predecessor[startIndex] = -1;

        while (!traversalQueue.isEmpty() && (index != targetIndex))
        {
            index = (traversalQueue.dequeue()).intValue();

            for (int i = 0; i < numVertices; i++)
            {
                if (hasEdge(index,i) && !visited[i])
                {
                    pathLength[i] = pathLength[index]  + 1;
                    predecessor[i] = index;
                    traversalQueue.enqueue(new Integer(i));
                    visited[i] = true;
                }
            }
        }
        if (index != targetIndex)
        {
            return resultList.iterator();
        }

        StackADT<Integer> stack = new LinkedStack<Integer>();
        index = targetIndex;
        stack.push(new Integer(index));
        do
        {
            index = predecessor[index];
            stack.push(new Integer(index));
        } while (index != startIndex);

        while (!stack.isEmpty()) {
            resultList.addToRear(((Integer)stack.pop()));
        }

        return resultList.iterator();
    }
    protected class GraphIterator implements Iterator
    {
        private int expectedModCount;
        private Iterator iter;

        /**
         * Sets up this iterator using the specified iterator.
         *
         * @param iter the list iterator created by a graph traversal
         */
        public GraphIterator(Iterator iter)
        {
            this.iter = iter;
            expectedModCount = modCount;
        }

        /**
         * Returns true if this iterator has at least one more element
         * to deliver in the iteration.
         *
         * @return  true if this iterator has at least one more element to deliver
         *          in the iteration
         * @throws ConcurrentModificationException if the collection has changed
         *          while the iterator is in use
         */
        @Override
        public boolean hasNext() throws ConcurrentModificationException
        {
            if (!(modCount == expectedModCount)) {
                throw new ConcurrentModificationException();
            }

            return (iter.hasNext());
        }

        /**
         * Returns the next element in the iteration. If there are no
         * more elements in this iteration, a NoSuchElementException is
         * thrown.
         *
         * @return the next element in the iteration
         * @throws NoSuchElementException if the iterator is empty
         */
        @Override
        public Object next() throws NoSuchElementException
        {
            if (hasNext()) {
                return (iter.next());
            } else {
                throw new NoSuchElementException();
            }
        }

        /**
         * The remove operation is not supported.
         *
         * @throws UnsupportedOperationException if the remove operation is called
         */
        @Override
        public void remove()
        {
            throw new UnsupportedOperationException();
        }
    }

    @Override
    public boolean isEmpty() {
        if (size() != 0){
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public boolean isConnected() {
        boolean result = true;
        for (int i = 0; i < numVertices;i++){
            int temp = 0;
            temp = getSizeOfIterator(iteratorBFS(i));
            if (temp != a){
                result = false;
                break;
            }
        }
        return result;
    }

    public  int getSizeOfIterator(Iterator iterator){
        int size = 0;
        while (iterator.hasNext()){
            size++;
            iterator.next();
        }
        return size;
    }
    @Override
    public int size() {
        return numVertices;
    }

    protected boolean indexIsValid(int index){
        if (index < size()){
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public String toString() {
        for (int i = 0;i < vertices.size();i++){
            VerticeNode node = vertices.get(i);
            System.out.print("顶点： " + node.getElement());

            while (node.getNext() != null){
                System.out.print("-" + node.getNext().getElement());
                node = node.getNext();
            }
            System.out.println(";");
        }
        return super.toString();
    }
}
