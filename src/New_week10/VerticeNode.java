package New_week10;

public class VerticeNode<T> {
    private VerticeNode<T> next;
    private T element;

    public VerticeNode(){
        next = null;
        element = null;
    }

    public VerticeNode(T elem){
        next = null;
        element = elem;
    }

    public VerticeNode<T> getNext(){
        return next;
    }

    public void setNext(VerticeNode<T> node){
        next = node;
    }

    public T getElement(){
        return element;
    }

    public void setElement(T elem){
        element = elem;
    }

    @Override
    public String toString() {
        VerticeNode<T> node = new VerticeNode<T>();
        String result = "";
        while (node != null){
            result += node.getElement() + " ";
            node = node.getNext();
        }
        return result;
    }
}
