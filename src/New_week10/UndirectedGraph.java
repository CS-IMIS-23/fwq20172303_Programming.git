package New_week10;

import New_week2.LinkedStack;
import New_week2.StackADT;
import New_week3.LinkedQueue;
import New_week3.QueueADT;
import New_week4.ArrayUnorderedList;
import New_week4.UnorderedListADT;

import java.util.*;

public class UndirectedGraph<T> implements GraphADT<T>
{
    protected final int DEFAULT_CAPACITY = 5;
    protected int numVertices;
    protected Double[][] adjMatrix;
    protected T[] vertices;
    protected int modCount;

    public UndirectedGraph() {
        numVertices = 0;
        this.adjMatrix = new Double[DEFAULT_CAPACITY][DEFAULT_CAPACITY];
        this.vertices = (T[]) (new Object[DEFAULT_CAPACITY]);
    }

    @Override
    public void addVertex(T vertex) {
        if ((numVertices + 1) == adjMatrix.length) {
            expandCapacity();
        }

        vertices[numVertices] = vertex;
        for (int i = 0; i <= numVertices; i++) {
            adjMatrix[numVertices][i] = Double.POSITIVE_INFINITY;
            adjMatrix[i][numVertices] = Double.POSITIVE_INFINITY;
        }
        numVertices++;
        modCount++;
    }

    @Override
    public void removeVertex(T vertex) {
        removeVertex(getIndex(vertex));
    }


    public void removeVertex(int index) {
        if (indexIsValid(index)) {
            for (int j = index; j < numVertices - 1; j++) {
                vertices[j] = vertices[j + 1];
            }
            vertices[numVertices - 1] = null;

            for (int i = index; i < numVertices - 1; i++) {
                for (int x = 0; x < numVertices; x++) {
                    adjMatrix[i][x] = adjMatrix[i + 1][x];
                }

            }
            for (int i = index; i < numVertices; i++) {
                for (int x = 0; x < numVertices; x++) {
                    adjMatrix[x][i] = adjMatrix[x][i + 1];
                }
            }

            for (int i = 0; i < numVertices; i++) {
                adjMatrix[numVertices][i] = Double.POSITIVE_INFINITY;
                adjMatrix[i][numVertices] = Double.POSITIVE_INFINITY;
            }
            numVertices--;
            modCount++;
        }
    }

    @Override
    public void addEdge(T vertex1, T vertex2) {
        addEdge(getIndex(vertex1), getIndex(vertex2));
    }

    private void addEdge(int index1, int index2) {

        if (indexIsValid(index1) && indexIsValid(index2)) {
            adjMatrix[index1][index2] = Double.POSITIVE_INFINITY;
            adjMatrix[index2][index1] = Double.POSITIVE_INFINITY;
            modCount++;
        }
    }


    public void addEdge(T vertex1, T vertex2, double weight) {
        addEdge(getIndex(vertex1), getIndex(vertex2), weight);
    }

    public void addEdge(int index1, int index2, double weight) {
        if (indexIsValid(index1) && indexIsValid(index2)) {
            adjMatrix[index1][index2] = weight;
            adjMatrix[index2][index1] = weight;
            modCount++;
        }
    }

    @Override
    public void removeEdge(T vertex1, T vertex2) {
        removeEdge(getIndex(vertex1), getIndex(vertex2));
    }

    public void removeEdge(int index1, int index2) {
        if (indexIsValid(index1) && indexIsValid(index2)) {
            adjMatrix[index1][index2] = Double.POSITIVE_INFINITY;
            adjMatrix[index2][index1] = Double.POSITIVE_INFINITY;
            modCount++;
        }
    }


    public int shortestPathLength(T startVertex, T targetVertex) {
        return shortestPathLength(getIndex(startVertex), getIndex(targetVertex));
    }

    public int shortestPathLength(int startIndex, int targetIndex) {
        int result = 0;

        if (!indexIsValid(startIndex) || !indexIsValid(targetIndex)) {
            return 0;
        }

        int index1;
        Iterator<Integer> it = iteratorShortestPathIndices(startIndex, targetIndex);

        if (it.hasNext()) {
            index1 = ((Integer) it.next()).intValue();
        } else {
            return 0;
        }

        while (it.hasNext()) {
            result++;
            it.next();
        }
        return result;
    }

    @Override
    public Iterator iteratorShortestPath(T startVertex, T targetVertex) {
        return iteratorShortestPath(getIndex(startVertex),
                getIndex(targetVertex));
    }

    public Iterator iteratorShortestPath(int startIndex, int targetIndex) {

        List<T> resultList = new ArrayList<T>();
        if (!indexIsValid(startIndex) || !indexIsValid(targetIndex)) {
            return resultList.iterator();
        }

        Iterator<Integer> it = iteratorShortestPathIndices(startIndex,
                targetIndex);
        while (it.hasNext()) {
            resultList.add(vertices[((Integer) it.next()).intValue()]);
        }
        return new GraphIterator(resultList.iterator());
    }

    protected Iterator<Integer> iteratorShortestPathIndices(int startIndex, int targetIndex) {
        int index = startIndex;
        int[] pathLength = new int[numVertices];
        int[] predecessor = new int[numVertices];
        QueueADT<Integer> traversalQueue = new LinkedQueue<Integer>();
        UnorderedListADT<Integer> resultList = new ArrayUnorderedList<Integer>();

        if (!indexIsValid(startIndex) || !indexIsValid(targetIndex) ||
                (startIndex == targetIndex)) {
            return resultList.iterator();
        }

        boolean[] visited = new boolean[numVertices];
        for (int i = 0; i < numVertices; i++) {
            visited[i] = false;
        }

        traversalQueue.enqueue(Integer.valueOf(startIndex));
        visited[startIndex] = true;
        pathLength[startIndex] = 0;
        predecessor[startIndex] = -1;

        while (!traversalQueue.isEmpty() && (index != targetIndex)) {
            index = (traversalQueue.dequeue()).intValue();

            for (int i = 0; i < numVertices; i++) {
                if (adjMatrix[index][i] < Double.POSITIVE_INFINITY && !visited[i]) {
                    pathLength[i] = pathLength[index] + 1;
                    predecessor[i] = index;
                    traversalQueue.enqueue(Integer.valueOf(i));
                    visited[i] = true;
                }
            }

        }
        if (index != targetIndex) {
            return resultList.iterator();
        }

        StackADT<Integer> stack = new LinkedStack<Integer>();
        index = targetIndex;
        stack.push(Integer.valueOf(index));
        do {
            index = predecessor[index];
            stack.push(Integer.valueOf(index));
        } while (index != startIndex);

        while (!stack.isEmpty()) {
            resultList.addToRear((stack.pop()));
        }

        return new NetworkIndexIterator(resultList.iterator());
    }

    public double shortestPathWeight(T vertex1, T vertex2) {
        return shortestPathWeight(getIndex(vertex1), getIndex(vertex2));
    }

    public double shortestPathWeight(int start, int end) {
        int[] previous = new int[numVertices];
        Double[] dist = new Double[numVertices];
        boolean[] flag = new boolean[numVertices];

        for (int i = 0; i < numVertices; i++) {
            flag[i] = false;
            previous[i] = 0;
            dist[i] = adjMatrix[start][i];
        }

        flag[start] = true;

        int k = 0;
        for (int i = 0; i < numVertices; i++) {

            Double min = Double.POSITIVE_INFINITY;
            for (int j = 0; j < numVertices; j++) {
                if (flag[j] == false && dist[j] < min && dist[j] != -1 && dist[j] != 0) {
                    min = dist[j];
                    k = j;
                }
            }
            flag[k] = true;

            for (int j = 0; j < numVertices; j++) {
                if (adjMatrix[k][j] != -1&&dist[j]!= -1) {
                    double temp = (adjMatrix[k][j] == Double.POSITIVE_INFINITY
                            ? Double.POSITIVE_INFINITY : (min + adjMatrix[k][j]));
                    if (flag[j] == false && (temp < dist[j])) {
                        dist[j] = temp;
                        previous[j] = k;
                    }
                }
            }
        }
        return dist[end];
    }

    @Override
    public Iterator iteratorBFS (T startVertex){
        return iteratorBFS(getIndex(startVertex));
    }

    public Iterator<T> iteratorBFS ( int startIndex){
        Integer x;
        QueueADT<Integer> traversalQueue = new LinkedQueue<Integer>();
        UnorderedListADT<T> resultList = new ArrayUnorderedList<T>();

        if (!indexIsValid(startIndex)) {
            return resultList.iterator();
        }

        boolean[] visited = new boolean[numVertices];
        for (int i = 0; i < numVertices; i++) {
            visited[i] = false;
        }

        traversalQueue.enqueue(startIndex);
        visited[startIndex] = true;

        while (!traversalQueue.isEmpty()) {
            x = traversalQueue.dequeue();
            resultList.addToRear(vertices[x]);
            for (int i = 0; i < numVertices; i++) {
                if (adjMatrix[x][i] < Double.POSITIVE_INFINITY && !visited[i]) {
                    traversalQueue.enqueue(i);
                    visited[i] = true;
                }
            }
        }
        return new NetworkIterator(resultList.iterator());
    }

    @Override
    public Iterator iteratorDFS (T startVertex){
        return iteratorDFS(getIndex(startVertex));
    }


    public Iterator<T> iteratorDFS ( int startIndex){
        Integer x;
        boolean found;
        StackADT<Integer> traversalStack = new LinkedStack<Integer>();
        UnorderedListADT<T> resultList = new ArrayUnorderedList<T>();
        boolean[] visited = new boolean[numVertices];

        if (!indexIsValid(startIndex)) {
            return resultList.iterator();
        }

        for (int i = 0; i < numVertices; i++) {
            visited[i] = false;
        }

        traversalStack.push(startIndex);
        resultList.addToRear(vertices[startIndex]);
        visited[startIndex] = true;

        while (!traversalStack.isEmpty()) {
            x = traversalStack.peek();
            found = false;

            for (int i = 0; (i < numVertices) && !found; i++) {
                if (adjMatrix[x][i] < Double.POSITIVE_INFINITY && !visited[i]) {
                    traversalStack.push(i);
                    resultList.addToRear(vertices[i]);
                    visited[i] = true;
                    found = true;
                }
            }
            if (!found && !traversalStack.isEmpty()) {
                traversalStack.pop();
            }
        }
        return new NetworkIterator(resultList.iterator());
    }


    @Override
    public boolean isEmpty () {
        if (numVertices == 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean isConnected () {
        boolean result = true;
        for (int i = 0; i < numVertices; i++) {
            int temp = 0;
            temp = getSizeOfIterator(this.iteratorBFS(vertices[i]));
            if (temp != numVertices) {
                result = false;
                break;
            }
        }
        return result;
    }

    public int getSizeOfIterator (Iterator iterator){
        int size = 0;
        while (iterator.hasNext()) {
            size++;
            iterator.next();
        }
        return size;
    }

    @Override
    public String toString() {
        if (numVertices == 0) {
            return "Graph is empty";
        }

        String result = new String("");

        result += "Adjacency Matrix\n";
        result += "---------------------------------------------\n";
        result += "index\t";

        for (int i = 0; i < numVertices; i++) {
            result += "" + i;
            if (i < 10) {
                result += "      ";
            }
        }
        result += "\n\n";

        for (int i = 0; i < numVertices; i++) {
            result += "" + i + "\t";

            for (int j = 0; j < numVertices; j++) {
                if (adjMatrix[i][j]<Double.POSITIVE_INFINITY) {
                    result += " "+ adjMatrix[i][j];
                } else {
                    result += " ----- ";
                }
            }
            result += "\n";
        }

        result += "\n\nVertex Values";
        result += "\n------------------------------------------\n";
        result += "index\tvalue\n\n";

        for (int i = 0; i < numVertices; i++) {
            result += "   " + i + "\t";
            result += vertices[i].toString() + "\n";
        }
        result += "\n";
        return result;
    }

    protected void expandCapacity () {
        T[] largerVertices = (T[]) (new Object[vertices.length * 2]);
        Double[][] largerAdjMatrix =
                new Double[vertices.length * 2][vertices.length * 2];

        for (int i = 0; i < numVertices; i++) {
            for (int j = 0; j < numVertices; j++) {
                largerAdjMatrix[i][j] = adjMatrix[i][j];
            }
            largerVertices[i] = vertices[i];
        }

        vertices = largerVertices;
        adjMatrix = largerAdjMatrix;
    }

    @Override
    public int size () {
        return numVertices;
    }

    protected boolean indexIsValid ( int index){
        if (index < size()) {
            return true;
        } else {
            return false;
        }
    }

    public int getIndex (T vertex){

        int index = numVertices - 1;

        for (int i = 0; i < numVertices; i++) {
            if (vertex.equals(vertices[i])) {
                index = i;
                break;
            }
        }
        return index;
    }

    protected class NetworkIterator implements Iterator<T> {
        private int expectedModCount;
        private Iterator<T> iter;

        public NetworkIterator(Iterator<T> iter) {
            this.iter = iter;
            expectedModCount = modCount;
        }

        @Override
        public boolean hasNext() throws ConcurrentModificationException {
            if (!(modCount == expectedModCount)) {
                throw new ConcurrentModificationException();
            }

            return (iter.hasNext());
        }

        @Override
        public T next() throws NoSuchElementException {
            if (hasNext()) {
                return (iter.next());
            } else {
                throw new NoSuchElementException();
            }
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    protected class NetworkIndexIterator implements Iterator<Integer> {
        private int expectedModCount;
        private Iterator<Integer> iter;

        public NetworkIndexIterator(Iterator<Integer> iter) {
            this.iter = iter;
            expectedModCount = modCount;
        }

        @Override
        public boolean hasNext() throws ConcurrentModificationException {
            if (!(modCount == expectedModCount)) {
                throw new ConcurrentModificationException();
            }

            return (iter.hasNext());
        }

        @Override
        public Integer next() throws NoSuchElementException {
            if (hasNext()) {
                return (iter.next());
            } else {
                throw new NoSuchElementException();
            }
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    protected class GraphIterator implements Iterator<T> {
        private int expectedModCount;
        private Iterator<T> iter;

        public GraphIterator(Iterator<T> iter) {
            this.iter = iter;
            expectedModCount = modCount;
        }

        @Override
        public boolean hasNext() throws ConcurrentModificationException {
            if (!(modCount == expectedModCount)) {
                throw new ConcurrentModificationException();
            }

            return (iter.hasNext());
        }

        @Override
        public T next() throws NoSuchElementException {
            if (hasNext()) {
                return (iter.next());
            } else {
                throw new NoSuchElementException();
            }
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    protected class GraphIndexIterator implements Iterator<Integer> {
        private int expectedModCount;
        private Iterator<Integer> iter;

        public GraphIndexIterator(Iterator<Integer> iter) {
            this.iter = iter;
            expectedModCount = modCount;
        }

        @Override
        public boolean hasNext() throws ConcurrentModificationException {
            if (!(modCount == expectedModCount)) {
                throw new ConcurrentModificationException();
            }

            return (iter.hasNext());
        }

        @Override
        public Integer next() throws NoSuchElementException {
            if (hasNext()) {
                return (iter.next());
            } else {
                throw new NoSuchElementException();
            }
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
