package New_week10;

import java.util.Iterator;

public class FlightNet<T> extends UndirectedGraph<T>{
    public FlightNet(){
        super();
    }

    // 添加城市（即顶点）
    public void addCity(T city){
        addVertex(city);
    }

    // 添加边（包括权重）
    @Override
    public void addEdge(T city1, T city2, double weight){
        addEdge(getIndex(city1),getIndex(city2),weight);
    }

    // 获取最短路径
    public void getShortesPath(T city1,T city2){
        int index1 = getIndex(city1);
        int index2 = getIndex(city2);

        // 如果选择的两个城市间没有路径，输出错误
        if (shortestPathLength(index1,index2) == 0){
            System.out.println(city1 + "与" + city2 + "之间无通路");
        }
        else {
            String result = "";
            Iterator iterator = iteratorShortestPath(index1,index2);
            while (iterator.hasNext()){
                result += iterator.next() + " ";
            }
            System.out.println("最短路径为： " + result);
            System.out.println("最短路径长度为： " + shortestPathLength(index1,index2));
        }
    }

    // 获取最小权重路径
    public void getShortesWeighPath(T city1,T city2){
        if (shortestPathLength(city1,city2) < Double.POSITIVE_INFINITY){
            System.out.println("最便宜的价格为： "+ shortestPathWeight(city1,city2));
        }
        else {
            System.out.println(city1 + "与" + city2 + "之间无通路");
        }
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
