package Experiment3;

public class Searching2 {
    // 线性查找
    public static <T> boolean linearSearch(T[] data, int min, int max, T target)
    {
        int index = min;
        boolean found = false;

        while (!found && index <= max)
        {
            found = data[index].equals(target);
            index++;
        }

        return found;
    }

    //  二分查找
    public static Comparable binarySearch(Comparable[] data, Comparable target) {
        Comparable result = null;
        int first = 0, last = data.length - 1, mid;

        while (result == null && first <= last) {
            mid = (first + last) / 2;
            if (data[mid].compareTo(target) == 0) {
                result = data[mid];
            } else if (data[mid].compareTo(target) > 0) {
                last = mid - 1;
            } else {
                first = mid + 1;
            }
        }
        return result;
    }

    // 插值查找
    public static int InsertionSearch(int[] a, int value, int low, int high) {
        int mid = low + (value - a[low]) / (a[high] - a[low]) * (high - low);
        if (a[mid] == value) {
            return mid;
        }
        if (a[mid] > value) {
            return InsertionSearch(a, value, low, mid - 1);
        } else {
            return InsertionSearch(a, value, mid + 1, high);
        }
    }

    // 斐波那契查找
    // 使用递归建立斐波那契数列
    public static int Fibonacci(int n) {
        if(n == 0) {
            return 0;
        }
        if(n == 1) {
            return 1;
        }
        return Fibonacci(n - 1) + Fibonacci(n - 2);
    }

    public static int FibonacciSearch(int[] data,int n,int key) {
        int low = 1;
        int high = n;
        int mid;

        // 寻找k值，k值要求k值能使得F[k]-1恰好大于或等于n
        int k = 0;
        while(n > Fibonacci(k) - 1)
        {
            k++;
        }
        //因为无法直接对原数组增加长度，所以定义一个新的数组
        //采用System.arraycopy()进行数组间的赋值
        int[] temp = new int[Fibonacci(k)];
        System.arraycopy(data, 0, temp, 0, data.length);
        //对数组中新增的位置进行赋值
        for(int i = n + 1;i <= Fibonacci(k) - 1;i++)
        {
            temp[i]=temp[n];
        }

        while(low <= high) {
            mid = low + Fibonacci(k - 1) - 1;
            // 在mid的左边进行查找
            if(temp[mid] > key) {
                high = mid - 1;
                k = k - 1;
            }
            // 在mid的右边进行查找
            else if(temp[mid] < key) {
                low = mid + 1;
                k = k - 2;
            }else {
                if(mid <= n) {
                    return mid;
                }
                //当mid位于新增的数组中时，返回n
                else {
                    return n;
                }
            }
        }
        return 0;
    }
}
