package Experiment3;

import Experiment3.Sorting2;
import New_week8.LinkedBinarySearchTree;
import junit.framework.TestCase;
import org.junit.Test;

import static org.junit.Assert.*;

public class Sorting2Test extends TestCase {

    @Test
    public void testselectionSort() {
        Comparable list[] = {1,3,6,13,17,20,23,81,99,2303};
        // 正常测试+边界测试
        Comparable list1[] = {2303,99,81,23,20,17,13,6,3,1};
        Sorting2.selectionSort(list1);
        assertEquals(list1[0],list[0]);
//        // 异常测试
//        Comparable listerror[] = null;
//        Sorting2.selectionSort(listerror);
//        assertEquals(listerror[0],list[0]);
    }

    @Test
    public void testinsertionSort() {
        Comparable list[] = {1,3,6,13,17,20,23,81,99,2303};
        // 正常测试+边界测试
        Comparable list2[] = {2303,99,81,23,20,17,13,6,3,1};
        Sorting2.insertionSort(list2);
        assertEquals(list2[0],list[0]);
//        // 异常测试
//        Comparable listerror[] = null;
//        Sorting2.insertionSort(listerror);
//        assertEquals(listerror[0],list[0]);
    }

    @Test
    public void testbubbleSort() {
        Comparable list[] = {1,3,6,13,17,20,23,81,99,2303};
        // 正常测试+边界测试
        Comparable list3[] = {2303,99,81,23,20,17,13,6,3,1};
        Sorting2.bubbleSort(list3);
        assertEquals(list3[0],list[0]);
//        // 异常测试
//        Comparable listerror[] = null;
//        Sorting2.bubbleSort(listerror);
//        assertEquals(listerror[0],list[0]);
    }

    @Test
    public void testmergeSort() {
        Comparable list[] = {1,3,6,13,17,20,23,81,99,2303};
        // 正常测试+边界测试
        Comparable list4[] = {2303,99,81,23,20,17,13,6,3,1};
        Sorting2.mergeSort(list4);
        assertEquals(list4[0],list[0]);
//        // 异常测试
//        Comparable listerror[] = null;
//        Sorting2.mergeSort(listerror);
//        assertEquals(listerror[0],list[0]);
    }

    @Test
    public void testquickSort() {
        Comparable list[] = {1,3,6,13,17,20,23,81,99,2303};
        // 正常测试+边界测试
        Comparable list5[] = {2303,99,81,23,20,17,13,6,3,1};
        Sorting2.quickSort(list5);
        assertEquals(list5[0],list[0]);
//        // 异常测试
//        Comparable listerror[] = null;
//        Sorting2.quickSort(listerror);
//        assertEquals(listerror[0],list[0]);
    }

    @Test
    public void testheapSort() {
        int list[] = {1,3,6,13,17,20,23,81,99,2303};
        // 正常测试+边界测试
        int list6[] = {2303,99,81,23,20,17,13,6,3,1};
        Sorting2.HeapSort(list6);
        assertEquals(list6[0],list[0]);
//        // 异常测试
//        int listerror[] = null;
//        Sorting2.HeapSort(listerror);
//        assertEquals(listerror[0],list[0]);
    }

    @Test
    public void testshellSort() {
        int list[] = {1,3,6,13,17,20,23,81,99,2303};
        // 正常测试+边界测试
        int list7[] = {2303,99,81,23,20,17,13,6,3,1};
        Sorting2.ShellSort(list7,10);
        assertEquals(list7[0],list[0]);
//        // 异常测试
//        int listerror[] = null;
//        Sorting2.ShellSort(listerror,1);
//        assertEquals(listerror[0],list[0]);
    }

    @Test
    public void testBinaryTreeSort(){
        String list[] = {"1","3","6","13","17","20","23","81","99","2303"};
        LinkedBinarySearchTree tree = new LinkedBinarySearchTree();
        tree.addElement(2303);
        tree.addElement(99);
        tree.addElement(81);
        tree.addElement(23);
        tree.addElement(20);
        tree.addElement(17);
        tree.addElement(13);
        tree.addElement(6);
        tree.addElement(3);
        tree.addElement(1);
        int a = tree.size();
        String[] b = new String[a];
        for (int i = 0;i < a;i++){
            b[i] = String.valueOf(tree.findMin());
            tree.removeMin();
        }
        // 正常测试+边界测试
        assertEquals(b[0],list[0]);
//        // 异常测试
//        String[] c = null;
//        assertEquals(c[0],list[0]);
    }
}