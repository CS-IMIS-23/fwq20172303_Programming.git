package Experiment3;

import New_week9.ArrayHeap;

public class Sorting2 {
    // 选择排序
    public static <T extends Comparable<T>> void selectionSort(T[] data)
    {
        int min;
        T temp;

        for (int index = 0; index < data.length-1; index++)
        {
            min = index;
            for (int scan = index+1; scan < data.length; scan++) {
                if (data[scan].compareTo(data[min])<0) {
                    min = scan;
                }
            }

            swap(data, min, index);
        }
    }

    private static <T extends Comparable<T>> void swap(T[] data, int index1, int index2)
    {
        T temp = data[index1];
        data[index1] = data[index2];
        data[index2] = temp;
    }

    // 插入排序
    public static <T extends Comparable<T>> void insertionSort(T[] data)
    {
        for (int index = 1; index < data.length; index++)
        {
            T key = data[index];
            int position = index;

            while (position > 0 && data[position-1].compareTo(key) > 0)
            {
                data[position] = data[position-1];
                position--;
            }

            data[position] = key;
        }
    }

    // 冒泡排序
    public static <T extends Comparable<T>> void bubbleSort(T[] data)
    {
        int position, scan;
        T temp;

        for (position =  data.length - 1; position >= 0; position--)
        {
            for (scan = 0; scan <= position - 1; scan++)
            {
                if (data[scan].compareTo(data[scan+1]) > 0) {
                    swap(data, scan, scan + 1);
                }
            }
        }
    }

    // 归并排序
    public static <T extends Comparable<T>> void mergeSort(T[] data)
    {
        mergeSort(data, 0, data.length - 1);
    }

    private static <T extends Comparable<T>> void mergeSort(T[] data, int min, int max)
    {
        if (min < max)
        {
            int mid = (min + max) / 2;
            mergeSort(data, min, mid);
            mergeSort(data, mid+1, max);
            merge(data, min, mid, max);
        }
    }

    private static <T extends Comparable<T>> void merge(T[] data, int first, int mid, int last)
    {
        T[] temp = (T[])(new Comparable[data.length]);

        int first1 = first, last1 = mid;
        int first2 = mid+1, last2 = last;
        int index = first1;


        while (first1 <= last1 && first2 <= last2)
        {
            if (data[first1].compareTo(data[first2]) < 0)
            {
                temp[index] = data[first1];
                first1++;
            }
            else
            {
                temp[index] = data[first2];
                first2++;
            }
            index++;
        }

        while (first1 <= last1)
        {
            temp[index] = data[first1];
            first1++;
            index++;
        }

        while (first2 <= last2)
        {
            temp[index] = data[first2];
            first2++;
            index++;
        }

        for (index = first; index <= last; index++) {
            data[index] = temp[index];
        }
    }

    // 快速排序
    public static <T extends Comparable<T>> void quickSort(T[] data)
    {
        quickSort(data, 0, data.length - 1);
    }

    private static <T extends Comparable<T>> void quickSort(T[] data, int min, int max)
    {
        if (min < max)
        {
            int indexofpartition = partition(data, min, max);
            quickSort(data, min, indexofpartition - 1);
            quickSort(data, indexofpartition + 1, max);
        }
    }

    private static <T extends Comparable<T>> int partition(T[] data, int min, int max)
    {
        T partitionelement;
        int left, right;
        int middle = (min + max) / 2;

        partitionelement = data[middle];
        swap(data, middle, min);

        left = min;
        right = max;

        while (left < right)
        {
            while (left < right && data[left].compareTo(partitionelement) <= 0) {
                left++;
            }

            while (data[right].compareTo(partitionelement) > 0) {
                right--;
            }

            if (left < right) {
                swap(data, left, right);
            }
        }
        swap(data, min, right);
        return right;
    }

    // 堆排序(第十二章）
    public static void HeapSort(int[] data)
    {
        ArrayHeap temp = new ArrayHeap();

        for (int i = 0; i < data.length; i++) {
            temp.addElement(data[i]);
        }

        int count = 0;
        while (!(temp.isEmpty()))
        {
            data[count] = (int) temp.removeMin();
            count++;
        }
    }

    // 希尔排序
    // 希尔排序是简单插入排序的改进版。它与插入排序的不同之处在于，它会优先比较距离较远的元素，希尔排序又叫缩小增量排序
    public static void ShellSort(int[] data, int length){
        int i = length;
        while (i > 1){
            // 动态定义间隔
            i = (i + 1) / 2;
            for (int j = 0;j < length - i;j++){
                // 当对应比较的两个数中前者大于后者时进行交换
                if (data[j + i] < data[j]){
                    int temp = data[j + i];
                    data[j + i] = data[j];
                    data[j] = temp;
                }
            }
        }
    }

    // 二叉树排序使用二叉查找树实现，直接写在了测试中
}
