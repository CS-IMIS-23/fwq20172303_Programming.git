package Experiment3;

import org.junit.Test;

import static org.junit.Assert.*;

public class Searching2Test {

    @Test
    public void testlinearSearch() {
        // 正常测试
        Comparable list[] = {1,23,456,77,2303};
        // 元素在所查找的范围内
        assertEquals(true, Searching2.linearSearch(list,0,3,77));
        assertEquals(true,Searching2.linearSearch(list,0,4,2303)); //边界测试
        // 元素不在所查找的范围内
        assertEquals(false,Searching2.linearSearch(list,1,3,1));
    }

    @Test
    public void testbinarySearch() {
        // 正常测试
        Comparable list[] = {1,2,3,4,5,6,7,23,456,77,2303};
        // 元素在所查找的范围内
        assertEquals(4,Searching2.binarySearch(list,4));
        assertEquals(2303,Searching2.binarySearch(list,2303)); //边界测试
    }

    @Test
    public void testinsertionSearch() {
        // 正常测试
        int list[] = {1,2,3,4,5,6,7,23,77,456,2303};
        // 元素在所查找的范围内
        assertEquals(1,Searching2.InsertionSearch(list,2,0,10));
        assertEquals(10,Searching2.InsertionSearch(list,2303,0,10)); //边界测试
    }

    @Test
    public void testfibonacciSearch() {
        // 正常测试
        int list[] = {1,23,456,77,2303};
        // 元素在所查找的范围内
        assertEquals(0,Searching2.FibonacciSearch(list,4,456));
        assertEquals(4,Searching2.FibonacciSearch(list,4,2303)); //边界测试
    }
}