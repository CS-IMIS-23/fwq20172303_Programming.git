package New_week6;

/**
 * Sorting demonstrates sorting and searching on an array 
 * of objects.
 *
 * @author Lewis and Chase
 * @version 4.0 
 */
public class Sorting2
{
	/**
     * Sorts the specified array of integers using the selection
     * sort algorithm.
     *
     * @param data the array to be sorted
     */
    public static <T extends Comparable<T>> 
		void selectionSort(T[] data)
    {
        int min,time = 0;
        T temp;
		long startTime = System.nanoTime();

        for (int index = 0; index < data.length-1; index++)
        {
        	time++;
            min = index;
            for (int scan = index+1; scan < data.length; scan++) {
				if (data[scan].compareTo(data[min])<0) {
					time++;
					min = scan;
				}
			}
            swap(data, min, index);
        }
        long endTime = System.nanoTime();
        long totalTime = endTime - startTime;
        System.out.println("比较次数： " + time);
        System.out.println("执行时间： " + totalTime + " ns");
    }
	
	/**
	 * Swaps to elements in an array. Used by various sorting algorithms.
	 * 
	 * @param data   the array in which the elements are swapped
	 * @param index1 the index of the first element to be swapped
	 * @param index2 the index of the second element to be swapped
	 */
	private static <T extends Comparable<T>> 
		void swap(T[] data, int index1, int index2)
	{
		T temp = data[index1];
		data[index1] = data[index2];
		data[index2] = temp;
	}
	
    /**
     * Sorts the specified array of objects using an insertion
     * sort algorithm.
     *
     * @param data the array to be sorted
     */
    public static <T extends Comparable<T>> 
		void insertionSort(T[] data)
    {
    	int time = 0;
		long startTime = System.nanoTime();

        for (int index = 1; index < data.length; index++)
        {
            T key = data[index];
            int position = index;
            time++;
			
            // shift larger values to the right 
            while (position > 0 && data[position-1].compareTo(key) > 0)
            {
                time++;
            	data[position] = data[position-1];
                position--;
            }
            data[position] = key;
        }
		long endTime = System.nanoTime();
		long totalTime = endTime - startTime;
		System.out.println("比较次数： " + time);
		System.out.println("执行时间： " + totalTime + " ns");
    }
	
    /**
     * Sorts the specified array of objects using a bubble sort
     * algorithm.
     *
     * @param data the array to be sorted
     */
    public static <T extends Comparable<T>> 
		void bubbleSort(T[] data)
    {
        int time = 0;
    	int position, scan;
        T temp;
		long startTime = System.nanoTime();
		
        for (position =  data.length - 1; position >= 0; position--)
        {
        	time++;
            for (scan = 0; scan <= position - 1; scan++)
            {
            	time++;
                if (data[scan].compareTo(data[scan+1]) > 0) {
					swap(data, scan, scan + 1);
				}
            }
        }
		long endTime = System.nanoTime();
		long totalTime = endTime - startTime;
		System.out.println("比较次数： " + time);
		System.out.println("执行时间： " + totalTime + " ns");
    }

	public static <T extends Comparable<T>> void gapSort(T[] data,int i){
		int position,scan;
		T temp;
		int b = data.length - 1;

		if (i % 2 != 0){
			for (int a = i;a >= 1;a = a - 2 ){
				for (position = data.length - 1;position >= 0;position--){
					for (scan = 0;scan <= data.length - a;scan++){
						int j = scan + a;
						if (j <= data.length - 1){
							if (data[scan].compareTo(data[scan + a]) > 0){
								swap(data,scan,scan + a);
							}
						}
						else if (j > data.length - 1){
							if (data[b].compareTo(data[b - a]) > 0){
								swap(data,b,b-a);
							}
						}
					}
				}
			}
		}else if (i % 2 == 0){
			for (int a = i;a >= 1;a = a - 3 ){
				for (position = data.length - 1;position >= 0;position--){
					for (scan = 0;scan <= data.length - a;scan++){
						int j = scan + a;
						if (j <= data.length - 1){
							if (data[scan].compareTo(data[scan + a]) > 0){
								swap(data,scan,scan + a);
							}
						}
						else if (j > data.length - 1){
							if (data[b].compareTo(data[b - a]) > 0){
								swap(data,b,b-a);
							}
						}
					}
				}
			}
		}


	}

    /**
     * Sorts the specified array of objects using the merge sort
     * algorithm.
     *
     * @param data the array to be sorted
     */
	public static <T extends Comparable<T>>
		void mergeSort(T[] data)
	{
		long startTime = System.nanoTime();
		mergeSort(data, 0, data.length - 1);
		long endTime = System.nanoTime();
		long totalTime = endTime - startTime;
		System.out.println("比较次数： " + time);
		System.out.println("执行时间： " + totalTime + " ns");

	}
	
    /**
	 * Recursively sorts a range of objects in the specified array using the
	 * merge sort algorithm.
     *
     * @param data the array to be sorted
     * @param min  the index of the first element 
     * @param max  the index of the last element
     */
	private static <T extends Comparable<T>>
		void mergeSort(T[] data, int min, int max)
	{
		if (min < max)
		{
			int mid = (min + max) / 2;
			mergeSort(data, min, mid);
			mergeSort(data, mid+1, max);
			merge(data, min, mid, max);
		}
	}
	
	/**
     * Merges two sorted subarrays of the specified array.
     *
     * @param data the array to be sorted
     * @param first the beginning index of the first subarray 
     * @param mid the ending index fo the first subarray
     * @param last the ending index of the second subarray
     */
	@SuppressWarnings("unchecked")

	private static int time = 0;
	private static <T extends Comparable<T>>
		void merge(T[] data, int first, int mid, int last)
	{
		T[] temp = (T[])(new Comparable[data.length]);
		
		int first1 = first, last1 = mid;  // endpoints of first subarray
		int first2 = mid+1, last2 = last;  // endpoints of second subarray
		int index = first1;  // next index open in temp array
		
		//  Copy smaller item from each subarray into temp until one
		//  of the subarrays is exhausted
		while (first1 <= last1 && first2 <= last2)
		{
			if (data[first1].compareTo(data[first2]) < 0)
			{
				temp[index] = data[first1];
				first1++;
				time++;
			}
			else
			{
				temp[index] = data[first2];
				first2++;
				time++;
			}
			index++;
		}
		
		//  Copy remaining elements from first subarray, if any
		while (first1 <= last1)
		{
			temp[index] = data[first1];
			first1++;
			index++;
			time++;
		}
		
		//  Copy remaining elements from second subarray, if any
		while (first2 <= last2)
		{
			temp[index] = data[first2];
			first2++;
			index++;
			time++;
		}
		
		//  Copy merged data into original array
		for (index = first; index <= last; index++) {
			data[index] = temp[index];
		}
   }

	/**
	 * Sorts the specified array of objects using the quick sort algorithm.
	 * 
	 * @param data the array to be sorted
	 */
	public static <T extends Comparable<T>> 
		void quickSort(T[] data)
	{
		long startTime = System.nanoTime();
		quickSort(data, 0, data.length - 1);
		long endTime = System.nanoTime();
		long totalTime = endTime - startTime;
		System.out.println("比较次数： " + time1);
		System.out.println("执行时间： " + totalTime + " ns");
	}
	
	/**
	 * Recursively sorts a range of objects in the specified array using the
	 * quick sort algorithm. 
	 * 
	 * @param data the array to be sorted
	 * @param min  the minimum index in the range to be sorted
	 * @param max  the maximum index in the range to be sorted
	 */
	private static <T extends Comparable<T>> 
		void quickSort(T[] data, int min, int max)
	{
		if (min < max)
		{
			// create partitions
			int indexofpartition = partition(data, min, max);
			
			// sort the left partition (lower values)
			quickSort(data, min, indexofpartition - 1);
			
			// sort the right partition (higher values)
			quickSort(data, indexofpartition + 1, max);
		}
	}
	
	/**
	 * Used by the quick sort algorithm to find the partition.
	 * 
	 * @param data the array to be sorted
	 * @param min  the minimum index in the range to be sorted
	 * @param max  the maximum index in the range to be sorted
	 */
	private static int time1 = 0;
	private static <T extends Comparable<T>> 
		int partition(T[] data, int min, int max)
	{
		T partitionelement;
		int left, right;
		int middle = (min + max) / 2;
		
		// use the middle data value as the partition element
		partitionelement = data[middle];
		// move it out of the way for now
		swap(data, middle, min);
		
		left = min;
		right = max;
		
		while (left < right)
		{
			// search for an element that is > the partition element
			while (left < right && data[left].compareTo(partitionelement) <= 0) {
				left++;
				time1++;
			}
			
			// search for an element that is < the partition element
			while (data[right].compareTo(partitionelement) > 0) {
				right--;
				time1++;
			}
			
			// swap the elements
			if (left < right) {
				swap(data, left, right);
			}
		}
		
		// move the partition element into place
		swap(data, min, right);

		return right;
	}
}

