package New_week6;

public class PP93 {
    public static void main(String[] args)
    {
        Contact[] friends = new Contact[7];

        friends[0] = new Contact("John", "Smith", "610-555-7384");
        friends[1] = new Contact("Sarah", "Barnes", "215-555-3827");
        friends[2] = new Contact("Mark", "Riley", "733-555-2969");
        friends[3] = new Contact("Laura", "Getz", "663-555-3984");
        friends[4] = new Contact("Larry", "Smith", "464-555-3489");
        friends[5] = new Contact("Frank", "Phelps", "322-555-2284");
        friends[6] = new Contact("Marsha", "Grant", "243-555-2837");

        Sorting.insertionSort(friends);
        for (Contact friend : friends) {
            System.out.println(friend);
        }
        System.out.println("选择排序：");
        Sorting2.selectionSort(friends);
        System.out.println("插入排序： ");
        Sorting2.insertionSort(friends);
        System.out.println("冒泡排序：");
        Sorting2.bubbleSort(friends);
        System.out.println("归并排序：");
        Sorting2.mergeSort(friends);
        System.out.println("快速排序：");
        Sorting2.quickSort(friends);

    }
}
