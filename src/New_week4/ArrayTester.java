package New_week4;

public class ArrayTester {
    public static void main(String[] args) {
        System.out.println("=======================有序列表========================");
        ArrayOrderedList orderedList = new ArrayOrderedList();
        //有序列表
        orderedList.add("a");
        orderedList.add("b");
        orderedList.add("c");
        orderedList.add("d");
        System.out.println(orderedList.toString());
        System.out.println(orderedList.first());
        System.out.println(orderedList.last());
        System.out.println(orderedList.isEmpty());
        System.out.println(orderedList.contains("a"));
        orderedList.remove("b");
        System.out.println(orderedList.toString());


        System.out.println("=======================无序列表========================");
        ArrayUnorderedList unorderedList = new ArrayUnorderedList();
        //无序列表
        unorderedList.addToFront("a");
        unorderedList.addToFront("b");
        unorderedList.addToFront("c");
        unorderedList.addToRear("d");
        unorderedList.addAfter("e","b");
        System.out.println(unorderedList.toString());
        System.out.println(unorderedList.size());
        System.out.println(unorderedList.last());
        unorderedList.remove("a");
        System.out.println(unorderedList.toString());
        unorderedList.removeFirst();
        System.out.println(unorderedList.toString());
        unorderedList.removeLast();
        System.out.println(unorderedList.toString());
    }
}
