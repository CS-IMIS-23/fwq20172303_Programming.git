package New_week4;


import New_week2.LinearNode;

/**
 * LinkedOrderedList represents a singly linked implementation of an 
 * ordered list.
 *
 * @author Lewis and Chase
 * @version 4.0
 */
public class LinkedOrderedList<T> extends LinkedList<T> 
         implements OrderedListADT<T>
{
    /**
     * Creates an empty list.
     */
    public LinkedOrderedList()
    {
        super();
    }

    /**
     * Adds the specified element to this list at the location determined by
	 * the element's natural ordering. Throws a NonComparableElementException 
	 * if the element is not comparable.
     *
     * @param element the element to be added to this list
     * @throws NonComparableElementException if the element is not comparable
	 */
    @Override
    public void add(T element) {
        if (!(element instanceof Comparable)) {
            throw new NonComparableElementException("LinkedList");
        }
        Comparable<T> comparableElement = (Comparable<T>) element;
        LinearNode node = new LinearNode(comparableElement);
        LinearNode last = head;
        for (int i = 1; i < count; i++) {
            last = last.getNext();
        }

        if (size() == 0) {//链表为空时
            head = node;
            count++;
            return;
        } else if (comparableElement.compareTo(head.getElement()) < 0) {//插入在链首
            node.setNext(head);
            head = node;
            count++;
            return;
        } else if (comparableElement.compareTo((T) last.getElement()) > 0) {//插入在链尾
            last.setNext(node);
            count++;
        } else {//插入在链中间
            LinearNode current = head;
            LinearNode temp = current.getNext();
            for (int i = 0; i < count - 1; i++) {
                if (comparableElement.compareTo((T) temp.getElement()) < 0) {
                    current.setNext(node);
                    node.setNext(temp);
                    count++;
                    return;
                }
                current = temp;
                temp = temp.getNext();
            }
        }
    }
}
