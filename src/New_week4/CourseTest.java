package New_week4;

public class CourseTest {
    public static void main(String[] args) {
        Course course1 = new Course("CS", 101, "Introduction to Programming", "A-");
        Course course2 = new Course("ARCH", 305, "Building Analysis", "A");
        Course course3 = new Course("GER", 210, "Intermediate German");
        Course course4 = new Course("CS", 320, "Computer Architecture");
        Course course5 = new Course("THE", 201, "The Theatre Experience");

        ArrayOrderedList list = new ArrayOrderedList();
        list.add(course1);
        list.add(course2);
        list.add(course3);
        list.add(course4);
        list.add(course5);

        System.out.println("课程管理");
        int a = list.size();
        for (int i = 0; i < a;i++){
            System.out.println(list.first());
            list.removeFirst();
        }
        System.out.println();
        System.out.println("CompareTo测试：（1说明前者的排序大于后者，-1说明后者的排序大于前者）");
        System.out.println("course1与course2: " + course1.compareTo(course2));
        System.out.println("course1: " + course1);
        System.out.println("course2: " + course2);
        System.out.println("course4与course1: " + course4.compareTo(course1));
        System.out.println("course4: " + course4);
        System.out.println("course1: " + course1);
    }
}
