package New_week4;


import New_week2.LinearNode;

/**
 * LinkedUnorderedList represents a singly linked implementation of an 
 * unordered list.
 *
 * @author Lewis and Chase
 * @version 4.0
 */
public class LinkedUnorderedList<T> extends LinkedList<T> 
         implements UnorderedListADT<T>
{
    /**
     * Creates an empty list.
     */
    public LinkedUnorderedList()
    {
        super();
    }

    /**
     * Adds the specified element to the front of this list.
     *
     * @param element the element to be added to the list
	 */
    @Override
    public void addToFront(T element)
    {
        LinearNode<T> node = new LinearNode<T>((T) element);
        if (head == null) {
            head = node;
            tail = node;
        }
        else {
            node.setNext(head);
            head = node;
        }
        count++;
    }
	
	/**
     * Adds the specified element to the rear of this list.
     *
     * @param element the element to be added to the list
	 */
    @Override
    public void addToRear(T element)
    {
        LinearNode node = new LinearNode(element);

        if (isEmpty()){
            head = node;
        }
        else {
            tail.setNext(node);
        }
        tail = node;
        count++;
    }
	
	
    /**
     * Adds the specified element to this list after the given target.
     *
     * @param  element the element to be added to this list
	 * @param  target the target element to be added after
	 * @throws ElementNotFoundException if the target is not found
	 */
    @Override
    public void addAfter(T element, T target)
    {
        LinearNode<T> node = new LinearNode<>(element);
        LinearNode<T> current = head;
        LinearNode<T> temp = current.getNext();

        for (int i = 0;i < count - 1;i++){
            if (target.equals(current.getElement())) {
                current.setNext(node);
                node.setNext(temp);
                count++;
                return;
            }
            current = temp;
            temp = temp.getNext();
        }
    }
}
