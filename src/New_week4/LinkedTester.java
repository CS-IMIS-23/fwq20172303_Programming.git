package New_week4;

public class LinkedTester {
    public static void main(String[] args) {
        System.out.println("=======================有序列表========================");
        //有序列表
        LinkedOrderedList orderedList = new LinkedOrderedList();
        orderedList.add("d");
        orderedList.add("b");
        orderedList.add("a");
        orderedList.add("c");

        System.out.println(orderedList.toString());
        System.out.println(orderedList.isEmpty());
        System.out.println(orderedList.size());
        System.out.println(orderedList.first());
        System.out.println(orderedList.last());
        System.out.println(orderedList.contains("a"));
        orderedList.removeLast();
        System.out.println(orderedList.toString());

        System.out.println("=======================无序列表========================");
        //无序列表
        LinkedUnorderedList unorderedList = new LinkedUnorderedList();
        unorderedList.addToFront("a");
        unorderedList.addToFront("b");
        unorderedList.addToFront("c");
        unorderedList.addToRear("d");
        unorderedList.addAfter("e","b");
        System.out.println(unorderedList.toString());
        unorderedList.remove("a");
        System.out.println(unorderedList.toString());
        unorderedList.removeFirst();
        System.out.println(unorderedList.toString());
        unorderedList.removeLast();
        System.out.println(unorderedList.toString());
    }
}
