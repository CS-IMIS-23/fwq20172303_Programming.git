package week9;

import java.util.Scanner;

public class PP1209 {
    public static void main(String[] args) {
        int a,tri[][];// 所产生的是一个平面而不是一条坐标轴，所以要使用二维数组
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a number: ");
        a = scanner.nextInt();
        tri=new int[a][a];
        for(int i=0;i<a;i++)
            for(int j=0;j<=i;j++)
                tri[i][j]=recursion(i,j);

        for(int i=0;i<a;i++)
        {
            System.out.println();
            for(int n=10-i;n>=1;n--)
                System.out.print(" ");
            for(int j=0;j<=i;j++)
                System.out.print(tri[i][j]+" ");
        }
    }
    public static int recursion(int i,int j)
    {
        if(j==0||j==i)
            return 1;
        else
            return recursion(i-1,j)+ recursion(i-1,j-1);

    }
}
