package week9.classtest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Scanner;

public class RecursionTest {
    public static void main(String[] args) throws IOException {
        int num;
        System.out.print("Enter a number: ");
        Scanner scanner = new Scanner(System.in);
        num = scanner.nextInt();

        int result = fact(num);
        System.out.println("The result is: " + result);
        //创建文件
        File file = new File("E:\\java\\Test","recursion.txt");
        if (!file.exists()){
            file.createNewFile();
        }
        //将数据写入文件
        OutputStream outputStream = new FileOutputStream(file);
        String b = "The result is " + String.valueOf(result);
        byte[] bytes = b.getBytes();
        outputStream.write(bytes);
    }

    /*通过递归来计算F（n)
    * n=0,F(n)=0
    * n=1,F(n)=1
    * n>=2,F(n)=F(n-1)+F(n-2)*/
    public static int fact(int n) {
        int result;
        if (n == 0) { return 0; }
        else if (n == 1){return 1;}
        else result = fact(n -1) + fact(n -2);
        return result;
    }
}
