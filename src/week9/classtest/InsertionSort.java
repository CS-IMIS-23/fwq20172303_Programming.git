package week9.classtest;

public class InsertionSort {
    public static void insertionSort(int[] list) {
        for (int index = 1; index < list.length; index++) {
            int key = list[index];
            int position = index;

            //  Shift larger values to the right
            while (position > 0 && key - (list[position - 1]) < 0) {
                if (list[position] != 0) {
                    list[position] = list[position - 1];
                    position--;}
                else
                    break;
            }

            list[position] = (int)key;
        }
    }
}
