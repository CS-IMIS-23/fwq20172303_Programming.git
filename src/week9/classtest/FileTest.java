package week9.classtest;

import java.io.*;
import java.nio.CharBuffer;
import java.nio.ReadOnlyBufferException;
import java.util.Scanner;

public class FileTest {
    public static void main(String[] args)  {
        //创建文件
        File file = new File("sort.txt");
        if (!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //将数据写入文件
        String num, str = "";
        String another = "y";
        String[] crazy = new String[20];
        int i = 0;
        while (another.equalsIgnoreCase("y")){
            System.out.print("Enter a number: ");
            Scanner scan = new Scanner(System.in);
            num = scan.nextLine();
            crazy[i] = num;
            str += num + " ";
            i++;
            System.out.print("Do you want to enter again? (y/n)");
            another = scan.next();
        }
        System.out.println(str);
        Writer writer = null;
        try {
            writer = new FileWriter(file);
            writer.write(str);
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //把字符串型数组转换成整型数组，使用插入排序将数组排序
        int[] lalala = new int[60];
        for (i = 0;i<str.length();i++) {
            if (crazy[i] != null)
                lalala[i] = Integer.parseInt(crazy[i]);
            else
                break;
        }
        InsertionSort.insertionSort(lalala);
        String str1 = "";
        for (int j = 0;j < str.length();j++) {
            if (lalala[j] != 0) {
                str1 += String.valueOf(lalala[j]) + " ";
            }
            else
                break;
        }
        System.out.println(str1);
        //将数据写入文件
        try {
            writer.write(10);
            writer.write(str1);
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
