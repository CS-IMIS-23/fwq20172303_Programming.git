package week9;

import java.util.Scanner;

public class PP1201 {
    public static void main(String[] args) {
        String str, another = "y";
        int left, right;

        while (another.equalsIgnoreCase("y")) {
            Scanner scan = new Scanner(System.in);
            System.out.println("Enter a potential palindrome:");
            str = scan.nextLine();
            if (isPalindrome(str))
                System.out.println("That string IS a palindrome.");
            else
                System.out.println("That string is NOT a palindrome.");

            System.out.println();
            System.out.print("Test another palindrom (y/n)? ");
            another = scan.nextLine();
        }
    }
    public static boolean isPalindrome(String str){
        if(str.length()==1)
            return true ;
        else if(str.length()==2)
        {
            if(str.charAt(0)==str.charAt(str.length()-1))
                return true ;
            else
                return false ;
        }
        else if(str.charAt(0)!=str.charAt(str.length()-1))
            return false ;
        else
            return isPalindrome(str.substring(1,str.length()-1)) ;
    }
}