package week9;

import java.util.Scanner;

public class PP1102 {
    public static void main(String[] args) throws StringTooLongException {
        final int MAX = 20;
        Scanner scanner = new Scanner(System.in);

        while (true) {
            StringTooLongException problem = new StringTooLongException("Input string is too long.");
            System.out.print("Enter a sentence(Enter 'DONE' to exist): ");
            String str = scanner.nextLine();
            if (str.equals("DONE"))
                break;

            try {
                if (str.length() > MAX)
                    throw problem;
                else
                    System.out.println("What you enter is " + str);
            } catch (StringTooLongException exception) {
                System.out.println("What you enter is " + str + ".(Out of 20)");
            }
        }
    }
}