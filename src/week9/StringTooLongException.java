package week9;

public class StringTooLongException extends Exception{
    StringTooLongException(String message){
        super(message);
    }
}
