#!/bin/bash
echo "**********************************************************" 
echo "*                     Author：Blackwall                  *"
echo "*                     Date：2018-03-14 00:54             *"
echo "*                     Mail:wangzq@besti.edu.cn           *"
echo "**********************************************************"

echo "Usage：New a folder, copy AutoCompileX.sh into this folder."
echo "execute the command with Bash: sh AutoCompileX.sh "

echo "\n"
if [ ! -d "src" ]; then
  mkdir src
fi

if [ ! -d "bin" ]; then
  mkdir bin
fi

read -p "Please input a Java program:" name

echo "Start Compiling............................................"
echo "\n" 
name2=${name}".java"

mv $name2 src
#echo $name2
javac -d bin src/$name2

echo "Compiled" 
echo "\n" 

echo "Start Compiling............................................"
echo "\n" 
echo "The Results are as follows：\n"
java -cp bin $name
echo "\n Done"
 


